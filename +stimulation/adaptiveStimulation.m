function [A,dataFile,resp] = adaptiveStimulation(A,cues,options,expInfo,dataFile,task,trial)

% -----------------------------------------------------------------------
% adaptiveStimulation.m starts the stimulation for the adaptive tasks
%                       {'calib','stair','painDetection'}, shows stimulation
%                        screens and initiates response screen
%   
%   SYNTAX:       [A,dataFile,resp] = adaptiveStimulation(A,cues,options,expInfo...
%                                                        dataFile,task,trial)
%
%   IN:           A:        integer, amplitude suggested by task
%                 cues:     struct, contains names of slides initiated in
%                                 initiate Visuals
%                 options:  struct, options the tasks will run with
%                 expInfo:  struct, contains key info on how the experiment is 
%                                 run instance, incl. keyboard number 
%                 dataFile: struct, data file initiated in initDataFile.m
%                 task:     string, task name {'calib','stair','painDetection'}
%                 trial:    integer, trial number
%
%   OUT:          A:        integer, amplitude initiated by stimulation device
%                 dataFile: struct, updated data file
%                 resp:     integer, response detected
%
%   SUBFUNCTIONS: startStim.m; Screen.m; wait2.m; logEvent.m; logData.m;
%                 stopStim.m; showResponseScreen.m
%
%   AUTHOR:     Coded by: Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%

%% PREPARE stimulation

% Warning Screen & Countdown
Screen('DrawTexture', options.screen.windowPtr, cues.stimulationStart, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showReadyScreen,options,expInfo,dataFile,trial);

if options.doCountdown
Screen('DrawTexture', options.screen.windowPtr, cues.countdown1, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.countdown,options,expInfo,dataFile,trial);

Screen('DrawTexture', options.screen.windowPtr, cues.countdown2, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.countdown,options,expInfo,dataFile,trial);

Screen('DrawTexture', options.screen.windowPtr, cues.countdown3, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
[~,~,dataFile]  = eventListener.commandLine.wait2(options.dur.countdown,options,expInfo,dataFile,trial);
end

% Start stimulating at amplitude A (mV)
stimulation.startStim(expInfo.pStim.pStim_serial,options.pStim.pStim_params,A,expInfo.pStim.mode);

dataFile        = eventListener.logEvent(task,'_amplitudeOn', dataFile,[],trial);
[~,dataFile]    = eventListener.logData(A,task,'amplitude',dataFile,trial);

% Wait till Amplitude is adjusted on device
[~,~,dataFile]  = eventListener.commandLine.wait2(options.dur.amp2stim,options,expInfo,dataFile,trial);

%% START stimulation

% Stimulation Screen
Screen('DrawTexture', options.screen.windowPtr, cues.stimulationOn , [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
tools.wait2(options.dur.stimBurst,options,expInfo,dataFile,trial);
dataFile       = eventListener.logEvent(task,'_stimOn',dataFile,[],trial);


%% END stimulation
% Adjust Amplitude for pVNS to zero
stimulation.stopStim(options.pStim.pStim_serial,options.pStim.pStim_params);
dataFile       = eventListener.logEvent(task,'_ampOff', dataFile,[],trial);

% Wait till Amplitude is adjusted on device
[~,~,dataFile] = tools.wait2(options.dur.deltawin2stim,options,expInfo,dataFile,trial);

% Stimulation off Screen
Screen('DrawTexture',options.screen.windowPtr, cues.stimulationOff , [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
dataFile       = eventListener.logEvent(task,'_stimOff',dataFile,[],trial);
[~,~,dataFile] = eventListener.commandLine.wait2(options.dur.showOff,options,expInfo,dataFile,trial);

%% RESPONSE
% Show Screen, detect and save response
dataFile           = eventListener.logEvent(task,'_responseOn',dataFile,[],trial);
[dataFile,rt,resp] = tools.showResponseScreen(cues,options,expInfo,dataFile,task,trial);
dataFile           = eventListener.logEvent(task,'_responseOff',dataFile,[],trial);
[~,dataFile]       = eventListener.logData(rt,task,'rt', dataFile,trial);
[~,dataFile]       = eventListener.logData(resp,task,'response', dataFile,trial);
[~,~,dataFile]     = eventListener.commandLine.wait2(options.dur.showScreen,options,expInfo,dataFile,trial);

end
