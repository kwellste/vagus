function [expInfo,options] = initStim(expInfo,options)
% SERIAL_INIT initialize serial port

% Set pStim device parameters
% assert(strcmp(mode,'verum') || strcmp(mode,'sham'),'Please specify a valid mode');

%if strcmp(mode,'verum')
options.pStim.pStim_params              = PStimDeviceParams;
options.pStim.pStim_params.EvenBurst    = 100;
options.pStim.pStim_params.OddBurst     = 70;
options.pStim.pStim_params.SoftStart    = 0;
pStim_set_stimulation_mode(options.pStim.pStim_params,'tri');
options.pStim.pStim_params.PStimLinked = 'True';
% elseif strcmp(mode,'sham')
%     pStim.pStim_params = PStimDeviceParams;
%     pStim.pStim_params.EvenBurst = 100;
%     pStim.pStim_params.OddBurst = 70;
%     pStim.pStim_params.SoftStart = 0;
%     pStim_set_stimulation_mode(pStim.pStim_params,'bi');
%     pStim.pStim_params.PStimLinked = 'True';   
% end

% Open COM Port
if isempty(COM)
    disp('No COM port specified, please plug in PrimeStimCon');
else
    % check if specified COM Port is still open and close it
    temp_instruments = instrfind;
    for l = 1:length(temp_instruments)
       if strcmp(temp_instruments(l).Port,COM)
           if strcmp(temp_instruments(l).Status,'open')
              fclose(temp_instruments(l));
           end
       end
    end
    
    % open serial port 
    expInfo.pStim.pStim_serial = serial(expInfo.pStim.COM,'BaudRate',57600);
    set(pStim.pStim_serial,'BytesAvailableFcnMode','byte');
    set(pStim.pStim_serial,'OutputBufferSize',31);
    set(pStim.pStim_serial,'InputBufferSize',48);
    set(pStim.pStim_serial,'BytesAvailableFcn',{@bytesAvailable_callback,options.pStim.pStim_params})
    fopen(expInfo.pStim.pStim_serial);

end


end


