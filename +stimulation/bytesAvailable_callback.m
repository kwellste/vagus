function bytesAvailable_callback(obj, event, pStim_paramsObj)
%PSTIM_BYTESAVAILABLE_CALLBACK updates pStim device status parameters

out = fread(obj);
disp(out);
pStim_set_params(pStim_paramsObj,'LinkID',[char(out(2)),char(out(3))]);
switch(hex2dec([char(out(4)),char(out(5))]))
    case 0
        pStim_set_params(pStim_paramsObj,'RFStickFound','True');
    case 3
        pStim_set_params(pStim_paramsObj,'LastXFerWasOK','True');
    case 4 
        pStim_set_params(pStim_paramsObj,'LastXFerWasOK','False');
    case 5
        pStim_set_params(pStim_paramsObj,'PStimLinked','True');
    case 6
        pStim_set_params(pStim_paramsObj,'PStimLinked','False');
    case 10
        voltage = hex2dec([char(out(21)),char(out(22)),char(out(23))])*3/1638;
        pStim_set_params(pStim_paramsObj,'BatteryVoltage',voltage);
    otherwise
        
end

end

