function startStim(sObj,device_paramsObj,amp,mode)
%PSTIM_STARTSTIM starts the stimulation on pStim device
%Args: (expInfo.pStim.pStim_serial,options.pStim.pStim_params,A,'verum')
%    sObj: serial communication object from struct pStim.pStim_seria
%    device_paramsObj: pStim parameter handle from struct pStim.pStim_params
%    amp: Amplitude in mV, here "A"
%    mode: type of stimulation(verum or sham) TO DO    
%device_paramsObj.LastXFerWasOK = 'Unkown';
%first_transmit = true;

assert(floor(amp) == amp,'amp ist not a valid integer');

    switch mode
        case 'verum'
        fprintf(sObj,'%s',['Z','10','10',sprintf('%0.4X',floor(amp/10*1.122)),sprintf('%0.4X',floor(amp/10*1.122)),...
        sprintf('%0.4X',floor(amp/10*1.122)),sprintf('%0.2X',device_paramsObj.EvenBurst), sprintf('%0.2X',device_paramsObj.OddBurst),...
        sprintf('%0.2X',device_paramsObj.SoftStart),sprintf('%0.2X',device_paramsObj.OddBurst),...
        sprintf('%0.2X',device_paramsObj.EvenBurst),sprintf('%0.2X',device_paramsObj.OddBurst),device_paramsObj.StimulationModeByte]);
         case 'sham'
        fprintf(sObj,'%s',['Z','10','10',sprintf('%0.4X',floor(amp/10*0.75)),'0000',...
        '0000',sprintf('%0.2X',device_paramsObj.EvenBurst), sprintf('%0.2X',device_paramsObj.OddBurst),...
        sprintf('%0.2X',device_paramsObj.SoftStart),sprintf('%0.2X',device_paramsObj.OddBurst),...
        sprintf('%0.2X',device_paramsObj.EvenBurst),sprintf('%0.2X',device_paramsObj.OddBurst),device_paramsObj.StimulationModeByte]);
   end
end

