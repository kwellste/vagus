function dataFile = constantStimulation(A,cues,options,expInfo,dataFile,task,maxDur)

% -----------------------------------------------------------------------
% constantStimulation.m stimulates for a few minutes (maxDur) in the 'stim'
%                       task, initiates questions and registers responses
%   
%   SYNTAX:       dataFile = constantStimulation(A,cues,options,expInfo,dataFile,task)
%
%   IN:           A:        integer, amplitude suggested by task
%                 cues:     struct, contains names of slides initiated in
%                                 initiate Visuals
%                 options:  struct, options the tasks will run with
%                 expInfo:  struct, contains key info on how the experiment is 
%                                 run instance, incl. keyboard number 
%                 dataFile: struct, data file initiated in initDataFile.m
%                 task:     string, task name, e.g. 'stim'
%                 maxDur:   integer, maximum duration this task should run, 
%                                     for, specified in specifyProtocol
%
%   OUT:          dataFile: struct, updated data file
%
%   SUBFUNCTIONS: startStim.m; Screen.m; wait2.m; logEvent.m; logData.m;
%                 stopStim.m; showResponseScreen.m
%
%   AUTHOR:     Coded by: Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%
%% INITIALIZE task
stimulating = 1;
nQs         = options.stim.questionFrequency;
qTimings    = nQuestions+1;
tic;

%% START Stimulation

% Start stimulating at amplitude A (mV)
stimulation.startStim(expInfo.pStim.pStim_serial,options.pStim.pStim_params,A,expInfo.pStim.mode);
dataFile = eventListener.logEvent(task,'_amplitudeOn', dataFile,[],[]);

% Wait till Amplitude is adjusted on device
[~,~,dataFile] = tools.wait2(options.dur.deltawin2stim,options,expInfo,dataFile,trial);

% Stimulation Screen
while stimulating
Screen('DrawTexture', options.screen.windowPtr, cues.stimulationOn ,[], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
[~,~,dataFile] = tools.wait2(maxDur, options,expInfo,dataFile,trial);
dataFile = eventListener.logEvent(task,'_stimOn', dataFile,[],[]);


%% get response
% Show Screen, detect and save response

time = GetSecs() - dataFile.event.stim_On;
trial = 1;
if time == ismember(time,[(maxDur/qTimings),(maxDur/(qTimings/nQs-2)),maxDur/(qTimings/(nQs-1)),(maxDur/(qTimings/nQs))])
     [dataFile,~,resp] = tools.showResponseScreen(cues,options,expInfo,dataFile,task,trial);
     trial + 1;
end

end


%% SAVE data
[~, dataFile] = eventListener.logData(resp,task,'response', dataFile,[]);

end
