function dataFile = simulateConstantStimulation(A,cues,options,expInfo,dataFile,task,maxDur)

% -----------------------------------------------------------------------
% simulateConstantStimulation.m simulates stimulating for a few minutes (maxDur)
%                              in the 'stim' task, initiates questions and 
%                              registers responses
%   
%   SYNTAX:       dataFile = constantStimulation(A,cues,options,expInfo,dataFile,task)
%
%   IN:           A:        integer, amplitude suggested by task
%                 cues:     struct, contains names of slides initiated in
%                                 initiate Visuals
%                 options:  struct, options the tasks will run with
%                 expInfo:  struct, contains key info on how the experiment is 
%                                 run instance, incl. keyboard number 
%                 dataFile: struct, data file initiated in initDataFile.m
%                 task:     string, task name, e.g. 'stim'
%                 maxDur:   integer, maximum duration this task should run, 
%                                     for, specified in specifyProtocol
%
%   OUT:          dataFile: struct, updated data file
%
%   SUBFUNCTIONS: startStim.m; Screen.m; wait2.m; logEvent.m; logData.m;
%                 stopStim.m; showResponseScreen.m
%
%   AUTHOR:     Coded by: Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%
%% INITIALIZE task
stimulating = 1;
nQs         = options.stim.questionFrequency;
qTimings    = nQuestions+1;
tic;

%% PREPARE Stimulation
% Warning Screen & Countdown
Screen('DrawTexture', options.screen.windowPtr, cues.stimulationStart, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
tools.wait2(options.dur.showReadyScreen,options,expInfo,dataFile,trial);

if options.doCountdown
Screen('DrawTexture', options.screen.windowPtr, cues.countdown1, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
tools.wait2(options.dur.countdown,options,expInfo,dataFile,trial);

Screen('DrawTexture', options.screen.windowPtr, cues.countdown2, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
tools.wait2(options.dur.countdown,options,expInfo,dataFile,trial);

Screen('DrawTexture', options.screen.windowPtr, cues.countdown3, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
[~,~,dataFile] = tools.wait2(options.dur.countdown,options,expInfo,dataFile,trial);
end

%% START Stimulation
% Wait till Amplitude is adjusted on device
[~,~,dataFile] = tools.wait2(options.dur.deltawin2stim,options,expInfo,dataFile,trial);

% Stimulation Screen
while stimulating
Screen('DrawTexture', options.screen.windowPtr, cues.stimulationOn ,[], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
[~,~,dataFile] = tools.wait2(maxDur, options,expInfo,dataFile,trial);

%% get response
% Show Screen, detect and save response

time = GetSecs() - dataFile.event.stim_On;
trial = 1;
if time == ismember(time,[(maxDur/qTimings),(maxDur/(qTimings/nQs-2)),maxDur/(qTimings/(nQs-1)),(maxDur/(qTimings/nQs))])
     [dataFile,rt,resp] = tools.showResponseScreen(cues,options,expInfo,dataFile,task,trial);
     trial + 1;
     Screen('DrawTexture', options.screen.windowPtr, cues.stimulationOn ,[], options.screen.rect, 0);
     Screen('Flip', options.screen.windowPtr);
end

%% END  Stimulation
tocID = toc;

if tocID> maxDur
    stimulating = 0;
    stimulation.stopStim(options.pStim.pStim_serial,options.pStim.pStim_params);
    dataFile = eventListener.logEvent(task,'_ampOff', dataFile,[],[]);
end

end
% Wait till Amplitude is adjusted on device
eventListener.commandLine.wait2(options.dur.deltawin2stim,options,expInfo,dataFile,trial);
dataFile = eventListener.logEvent(task,'_stimOff',dataFile,[],[]);

% Stimulation off Screen
Screen('DrawTexture',options.screen.windowPtr, cues.thankYou , [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
tools.wait2(options.dur.showOff,options,expInfo,dataFile,trial);

Screen('DrawTexture',options.screen.windowPtr, cues.stimulationOff , [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
tools.wait2(options.dur.showOff,options,expInfo,dataFile,trial);


%% SAVE data
[~, dataFile] = eventListener.logData(rt,task,'rt', dataFile,[]); 
[~, dataFile] = eventListener.logData(resp,task,'response', dataFile,[]);

end
