function stopStim(sObj,device_paramsObj)
%PSTIM_STOPSTIM stops the stimulation on pStim device
%Args: 
%    sObj: serial communication object
%    device_paramsObj: pStim parameter handle

fprintf(sObj,'%s',['Z','10','10','00000000000000000000000000']);

end

