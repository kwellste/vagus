classdef PStimDeviceParams < handle 
    %PSTIMDEVICEPARAMS pStim device status parameters   
    properties
        LinkID
        RFStickFound
        PStimLinked
        LastXFerWasOK
        BatteryVoltage
        Amplitude
        EvenBurst
        OddBurst
        SoftStart
        StimulationMode
        StimulationModeByte
    end
    
    methods
        function pStim_set_params(obj,property,value)
           [obj.(property)] = value; 
        end
        
        function pStim_set_stimulation_mode(obj,mode)
           switch mode
               case 'mono'    
                   obj.StimulationMode = 'mono';
                   obj.StimulationModeByte = '0B';
               case 'bi'
                   obj.StimulationMode = 'bi';
                   obj.StimulationModeByte = '0D';
               case 'tri'                  
                   obj.StimulationMode = 'tri';
                   obj.StimulationModeByte = '0F';
           end
        end
    end
end

