function [expMode,PPID,visitNo,stairType] = vagus_study
%% _______________________________________________________________________________%
% vagus_study.m allows the experimenter to specify information on the mode the 
%               task will be run in, the participant's PPID, and the visit number.
%               The latter will be converted to the protocol type, depending 
%               on the PPID. This ensures the anonymous randomisation of study 
%               visits per PPID as defined a-priori by an external entity.
%
% SYNTAX:  [expMode,PPID,visitNo,stairType] = vagus_study
%
% OUT:      expMode: - In 'debug' mode timings are shorter, and the experiment
%                     won't be full screen. You may use breakpoints.
%                   - In 'experiment' mode you are running the entire
%                     experiment as it is intended
%
%           PPID:    A 4-digit integer (0001:0999) PPIDs have
%                   been assigned to participants a-priori
%
%           visitNo: A 1-digit integer (1:4). Each participant will be doing
%                   this task 4 times.
%
%  AUTHOR:  Coded by: Katharina V. Wellstein, December 2019
% _______________________________________________________________________________%

%% PREPARE environment
close all;
clearvars;
clc; 

%% SPECIFY inputs
expMode     = input('Enter ''debug'' or ''experiment'' ','s');
stairType   = input('Enter ''simpleUp'' or ''simpleDown'' or ''interleaved'' ','s');
PPID        = input('TNU_VAGUS_','s');
visitNo     = input('Visit Number '); 

%% Check if inputs are correct

if strcmp(expMode, 'debug')     % expMode check
   disp('You are running VAGUS in DEBUG mode'); 
    elseif strcmp(expMode, 'experiment')
           disp('You are running VAGUS in EXPERIMENT mode');
    else
        expMode = input('Your input is not correct, type either ''debug'' or ''experiment'' :','s');
end                             % END of mode check

if strcmp(stairType, 'simpleUp')% stairType check
   disp('You are running the VAGUS study with a simple Upward staircase'); 
    elseif strcmp(stairType, 'simpleDown')
           disp('You are running the VAGUS study with a simple Downward staircase');
    elseif strcmp(stairType, 'interleaved')
           disp('You are running the VAGUS study with an interleaved staircase');
    else
        stairType = input('Your input is not correct, type either ''simpleUp'' or ''simpleDown'' or ''interleaved'' :','s');
end % END stairType check

if ~numel(PPID) == 4 % PPID check
    disp('PPID has to be a 4 digit string');
end                  % END PPID check
    
if isnumeric(visitNo) && visitNo < 5 && visitNo > 0 % visitNo check
    fprintf('This is visit number %d ',visitNo); 
    else
        visitNo = input('Visit Number needs to be a numberic value between 1 and 4! Please enter the visit number:');   
end %END visitNo check


%% RUN experiment

vagus_experiment(expMode,PPID,visitNo,stairType);

end