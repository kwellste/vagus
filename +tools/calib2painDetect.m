function dataFile = calib2painDetect(dataFile)

% -----------------------------------------------------------------------
% calib2painDetect.m saves calibration task data to painDetection task data
%
%   SYNTAX:       dataFile = calib2painDetect(dataFile)
%
%   IN:           dataFile: struct, contains all variables, for which data
%                                   will be saved
%
%   OUT:          dataFile: struct, updated data file
%
%   AUTHOR:     Coded by: Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%
idx = find(dataFile.painDetect.amplitude(:,1),1);

dataFile.painDetect.rt              = [dataFile.calib.rt(1:idx-1); dataFile.painDetect.rt(idx:end)];
dataFile.painDetect.amplitude       = [dataFile.calib.amplitude(1:idx-1,1:2); dataFile.painDetect.amplitude(idx:end,1:2)];
dataFile.painDetect.response        = [dataFile.calib.response(1:idx-1,1:2); dataFile.painDetect.response(idx:end,1:2)];
dataFile.painDetect.detectThreshold = dataFile.calib.detectThreshold;

dataFile.calib.rt                   = [];
dataFile.calib.amplitude            = zeros(1000,2);
dataFile.calib.response             = zeros(1000,2);
dataFile.calib.detectThreshold      = [];

end