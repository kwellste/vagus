function dataFile = showPainCalibIntros(dataFile,options,cues,expInfo)

% -----------------------------------------------------------------------
% showPainCalibIntros.m shows introduction slides to this task
%
%   SYNTAX:     dataFile = showIntro(dataFile,options,cues,expInfo)
%
%   IN:         dataFile:struct, data file initiated in initDataFile.m
%               options: struct, options the tasks will run with
%               cues:    struct, contains names of slides initiated in
%                                initiate Visuals
%               expInfo: struct, contains key info on how the experiment is 
%                                run instance, incl. keyboard number 
%
%   OUT:        dataFile:struct, updated data file
%
%   SUBFUNCTIONS: Screen.m; wait2.m; waitfornextkey.m; logEvent.m
%
%   AUTHOR:     Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%

doCalib = isempty(dataFile.painDetect.amplitude);

if doCalib ==1
    Screen('DrawTexture', options.screen.windowPtr, cues.calib1, [], options.screen.rect, 0);
    Screen('Flip', options.screen.windowPtr);
    eventListener.commandLine.wait2(options.dur.showOff,options,expInfo,dataFile,1);

    Screen('DrawTexture', options.screen.windowPtr, cues.calib2, [], options.screen.rect, 0);
    Screen('Flip', options.screen.windowPtr);
    dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);
    
else
    Screen('DrawTexture', options.screen.windowPtr, cues.painDetect1_1, [], options.screen.rect, 0);
    Screen('Flip', options.screen.windowPtr);
    eventListener.commandLine.wait2(options.dur.showOff,options,expInfo,dataFile,1);

    Screen('DrawTexture', options.screen.windowPtr, cues.painDetect1_1_2, [], options.screen.rect, 0);
    Screen('Flip', options.screen.windowPtr);
    dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);
end

end