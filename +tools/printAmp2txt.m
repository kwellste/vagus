function printAmp2txt(amp, mode)
% PVNS_PRINTAMP2TXT prints a txt file that contains an amplitude for pVNS stimulation (3 digits at the moment)
% input     - amp: amplitude, e.g. 1000
%           - pathname: path to the amplitudefile
%           - type: 'verum' or 'sham' for the type of stimulation
%           - duration: wait time after changing the amplitude in the text
%           file

fileID = fopen(stimulation.StimSettings.exe,'w');  % this path needs to be the path of our .exe - needs to be adapted on site 

switch mode
    case 'verum'
        fprintf(fileID,['Matlab Template;' '%3d;' '%3d;' '%3d' ';100;100;100;100;100;100;120;20;0;True;False;False;False;True;False;False;False;False;False;False;True;False;'], [amp amp amp]);
    case 'sham'
        fprintf(fileID,['Matlab Template;' '%3d; %3d;' '%3d;' '100;100;100;100;100;100;120;20;0;False;True;False;False;True;False;False;True;False;False;False;True;False;'], [amp]);
end

% the template needs to have a fixed format 
% default is given below
% the planed settings are given above

% this.TemplateName   =   "New Template";
% this.Ch1Amplitude   =   100;
% this.Ch2Amplitude   =   100;
% this.Ch3Amplitude   =   100;
% this.BurstEvenCh1   =   30;
% this.BurstEvenCh2   =   30;
% this.BurstEvenCh3   =   30;
% this.BurstOddCh1    =   10;
% this.BurstOddCh2    =   10;
% this.BurstOddCh3    =   10;
% this.ActiveTime     =   120;		
% this.InactiveTime   =   20;	    	
% this.OffsetTime     =   0;			
% this.iStimSchema_Triphasisch  = false;
% this.iStimSchema_Biphasisch   = false;
% this.iStimSchema_Monophasisch = true;
% this.iStimModus_ConstantCurrent = false;
% this.iStimModus_ConstantVoltage = true;
% this.iStimModus_SumI0           = false;
% this.iStimModus_AdaptivI        = false;
% this.Individual  = false;
% this.SoftStart   = true;
% this.Ch2Invers   = false;
% this.Ch3Invers   = false;
% this.iModeVerum   = true;
% this.iModePlacebo = false;

fclose(fileID);
end