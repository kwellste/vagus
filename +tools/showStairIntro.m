function dataFile = showStairIntro(dataFile,options,cues,expInfo)

% -----------------------------------------------------------------------
% showStairIntro.m shows introduction slides to this task
%
%   SYNTAX:     dataFile = showIntro(dataFile,options,cues,expInfo)
%
%   IN:         dataFile:struct, data file initiated in initDataFile.m
%               options: struct, options the tasks will run with
%               cues:    struct, contains names of slides initiated in
%                                initiate Visuals
%               expInfo: struct, contains key info on how the experiment is 
%                                run instance, incl. keyboard number 
%
%   OUT:        dataFile:struct, updated data file
%
%   SUBFUNCTIONS: Screen.m; wait2.m; waitfornextkey.m; logEvent.m
%
%   AUTHOR:     Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%
%% INTRODUCE experiment

% Slide 1
Screen('DrawTexture', options.screen.windowPtr, cues.stair1, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
[~,~,dataFile] = eventListener.commandLine.wait2(options.dur.showIntroScreen,options,expInfo,dataFile,[]);
Screen('DrawTexture', options.screen.windowPtr, cues.stair2, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);

% Slide 2
Screen('DrawTexture', options.screen.windowPtr, cues.stair3, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
[~,~,dataFile] = eventListener.commandLine.wait2(options.dur.showIntroScreen,options,expInfo,dataFile,[]);
Screen('DrawTexture', options.screen.windowPtr, cues.stair4, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);

% Slide 3
Screen('DrawTexture', options.screen.windowPtr, cues.stair5, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
[~,~,dataFile] = eventListener.commandLine.wait2(options.dur.showIntroScreen,options,expInfo,dataFile,[]);
Screen('DrawTexture', options.screen.windowPtr, cues.stair6, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);

end