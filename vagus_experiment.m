function vagus_experiment(expMode,PPID,visitNo,stairType)
%% _______________________________________________________________________________%
%
% VAGUS_EXPERIMENT is the main function of the vagus nerve stimulation 
%                  experiment using LabChart & PrimeStim
%
% SYNTAX: vagus_experiment(expMode,PPID,visitNo)
%
% IN:     expMode:  In 'debug' mode timings are shorter, and the experiment
%                   won't be full screen. You may use breakpoints.
%                   In 'experiment' mode you are running the entire
%                   experiment as it is intended
%
%          PPID:    A 4-digit integer (0001:0999) PPIDs have
%                   been assigned to participants already
%
%          visitNo: A 1-digit integer (1:4). Each participant will be doing
%                   this task 4 times.
% 
% CODE:   Katharina V. Wellstein, 20.11.2019
%         
% _______________________________________________________________________________%

%% Log Command Window text to file
diaryName = [PPID datestr(now,'yyyymmdd')];
diary diaryName;
diary on;

%% Experiment structure 
% make structure with information regarding the tasks, structure and
% timings in the task (protocol) and save basic information on this
% experimental run (expInfo)

protocol = eventCreator.specifyProtocol;
expInfo  = output.saveExpInfo(expMode,PPID,visitNo,stairType);

%% Initialize additional software

% PsychToolbox
addpath(genpath('/Users/kwellste/git/VAGUS/toolboxes'));
PsychDefaultSetup(2);

switch expMode
    case 'experiment'
         % LabChart
         MATLAB_Sampling.m 
         % PsychToolbox
         HideCursor(0)
end

%% Prepare environment
options = eventCreator.specifyOptions(expMode,stairType);
expInfo = tools.prepEnvironment(expInfo);

%% Initiate stimulation device
if options.doInitStim
    [expInfo,options] = stimulation.initStim(expInfo,options);
end

%% Initialize output file
dataFile = output.initDataFile;

% %% Prove consistency of Experiment
% experiment.checkTimeLine(stages, experimentInfo);

%% prepare screen and visuals
options = eventCreator.initScreen(options);
cues    = eventCreator.initVisuals(options);

%% Start experiment
if options.doIntro
dataFile = tasks.showIntro(dataFile,options,cues,expInfo);
end

%% Run through the tasks of this experiment
expInfo.startTime = tic;
[dataFile,expInfo,protocol] = tasks.runExperiment(cues,options, protocol, expInfo, dataFile);
toc(expInfo.stopTime);

%% write data into file
save(fullfile(['+output/',expInfo.PPID,'/dataFile.mat']));
save(fullfile(['+output/',expInfo.PPID,'/expInfo.mat']));
save(fullfile(['+output/',expInfo.PPID,'/protocol.mat']));

diary off
save(['+output/+expLog',diaryname]);

end
