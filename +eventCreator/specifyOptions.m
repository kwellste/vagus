function options = specifyOptions(expMode,stairType)

% -----------------------------------------------------------------------
% specifyOptions.m creates structs for the different stages in the task
%                  Change this file if you would like to change task settings
%
%   SYNTAX:     options = specifyOptions(expMode,stairType)
%
%   IN:         expMode:   string, 'debug' or 'experiment'
%               stairType: string, 'simpleUp','simpleDown','interleaved'
%
%   OUT:        options: struct containing general and task specific
%                        options
%
%   AUTHOR:     Katharina V. Wellstein, December 2019
%
% -------------------------------------------------------------------------
%
%% specifing what steps will be executed
% Default is that everything will be executed and is thus set to 1 instead of 0.

switch expMode
    case 'experiment'
    
        % preparation
        options.doIntro          = 1;
        options.doInitStim       = 1;
        options.doStopStim       = 1;
        options.doStartLabChart  = 1;
        options.doStopLabChart   = 1;
        options.doKeyboard       = 1;
        options.doPlot           = 0;
        options.doCountdown      = 1;
        
        % stages
        options.doPainDetection  = 1;
        options.doCalibration    = 1;
        options.doRest           = 1;
        options.doStaircase      = 1; 
        options.doStimulation    = 1;
        options.doBreak          = 1;
        options.doShowResults    = 1;
        
        % durations
        options.screen.nIntroSlides = 6;     % no. intro slides
        options.screen.rect         = [0, 0, 1200, 600];
        screens                     = Screen('Screens');
        options.screen.number       = max(screens);

    case 'debug'
        
        % preparation
        options.doIntro          = 1;
        options.doInitStim       = 0;
        options.doStopStim       = 0;
        options.doStartLabChart  = 0;
        options.doStopLabChart   = 0;
        options.doKeyboard       = 1;
        options.doPlot           = 1;
        options.doCountdown      = 0;
        
        % stages
        options.doPainDetection  = 1;
        options.doCalibration    = 0;
        options.doRest           = 0;
        options.doStaircase      = 1; 
        options.doStimulation    = 0;
        options.doBreak          = 0;
        options.doShowResults    = 0;
        
        % durations
        options.screen.nIntroSlides = 6;     % no. intro slides
        options.screen.rect         = [20, 10, 900, 450];
        screens                     = Screen('Screens');
        options.screen.number       = max(screens);   
end

%% options screen
options.screen.white = WhiteIndex(options.screen.number);
options.screen.black = BlackIndex(options.screen.number);
options.screen.grey  = options.screen.white / 2;
options.screen.inc   = options.screen.white - options.screen.grey;

%% options stimulation
% will be filled by function initStim.m
options.pStim.pStim_params              = [];
options.pStim.pStim_params.EvenBurst    = [];
options.pStim.pStim_params.OddBurst     = [];
options.pStim.pStim_params.SoftStart    = [];

%% options PainDetection
options.painDetect.amplitudeStart = 0; % detection threshold will be added to this
options.painDetect.amplitudeMax   = 1000; % max amplitude of device
options.painDetect.stepSize       = 50;

%% options Calibration
options.calib.amplitudeStart = []; %will receive A from staircase
options.calib.amplitudeMax   = 1000; %will receive A from painDetection
options.calib.stepSize       = 50;

%% options Staircase
options.stair.nStepSameDir   = 3;  % number of steps before change of direction
options.stair.nStepsNewDir   = 1;                                        %%? %maybe differerent names
options.stair.minStep        = 10; % minimum stepsize in mV, steps of 10mV are most robust
[ratioStep, options.stair.T, options.stair.ratio] = eventCreator.getParamsStaircase(options.stair.nStepSameDir, options.stair.minStep);
options.stair.stairDirStep   = options.stair.minStep;
options.stair.otherDirStep   = ratioStep;
options.stair.stopCriterion  = 'reversals'; % 'reversals' or 'trials'
options.stair.minIterations  = 10; % 40 needs to be discussed             %% TO DO
options.stair.maxIterations  = 250; % 100 needs to be discussed          %% TO DO
options.stair.minValue       = 0;   % min amplitude allowed with current device
options.stair.maxValue       = 800; % max amplitude allowed with current device --> was 1000 SI
options.stair.reversals      = 0;
options.stair.direction      = [];
options.stair.reachedStop    = 0;

switch stairType
    case 'simpleUp'
        options.stair.startValue     = 0;
        options.stair.stopRule       = 5; % no. of times stopCriterion should be reached before stop

    case 'simpleDown'
        options.stair.startValue     = 600;
        options.stair.stopRule       = 5; % no. of times stopCriterion should be reached before stop
        
    case  'interleaved'
        options.stair.startValueUp   = 0;
        options.stair.startValueDown = 600;
        options.stair.stopRule       = 10; % no. of times stopCriterion should be reached before stop
end

%% options Stimulation
options.stim.questionFrequency       = 4;

%% options keyboard

options.keys.yes              = KbName('j'); 
options.keys.no               = KbName('n');
options.keys.next             = KbName('w');
options.keys.escape           = KbName('ESCAPE');

%% options duration of events

options.dur.waitnxtkeypress   = 5000; % in ms
options.dur.showScreen        = 3000;
options.dur.showIntroScreen   = 10000;
options.dur.showReadyScreen   = 2000;
options.dur.countdown         = 1000;
options.dur.showOff           = 1000; 
options.dur.endWait           = 2000; 
options.dur.stimBurst         = 2000;
options.dur.showResponse      = 1000; %5000
options.dur.deltawin2stim     = 100;  %1500 with Lenovo laptop
options.dur.rtTimeout         = 100;
options.dur.amp2stim          = 100;   % wait to chang the amplitude in the text file
options.dur.stim              = 60000; %  duration stimulation task 

%% messages

options.messages.abortText    = 'Das experiment wurde abgebrochen';
options.messages.timeOut      = 'Diese Aufgabe dauert nun leider bereits zu lange. Wir m??ssen bei der n??chsten Aufgabe weitermachen';

end