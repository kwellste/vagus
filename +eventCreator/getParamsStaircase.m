function [ratioStep,T,ratio] = getParamsStaircase(nStepSameDir,minStep)

%% pvns_getParamsStaircase estimates the optimal parameters 
% according to Chapter 5, Kingdom & Prins, Psychophysics 2016
%

% IN:    sameStep
%        minStep


% OUT:   stepUp
%        threshold
%        ratio

% -----------------------------------------------------------------------
% getParamsStaircase.m estimates the optimal amplitude parameters for the
%                     staircase based on Chapter 5, Kingdom & Prins, Psychophysics 2016
%   
%   SYNTAX:       [ratioStep,T,ratio] = getParamsStaircase(nStepSameDir,minStep)
%
%   IN:           nStepSameDir: integer, number of steps the staircase should
%                                        step at the same amplitude before
%                                        stepping up or down
%                 minStep:      integer, minimal stepsize staircase should take
%
%   OUT:          ratioStep:    integer, optimal stepSize for stepping in
%                                        other direaction of staircase
%                 T:            double,  percentage of detection which will be
%                                        evaluated using these parameters
%                 ratio:        double,  type of ratio used in this study
%
%
%   AUTHOR:     Coded by: Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%

%% CHECK input
if nStepSameDir>4
    error('D value is too large. Maximum step difference allowed is 4.')
end

%% INITIATE 
% values based on Kingdom & Prins, Psychophysics 2016

% Rule            1Up/1Down, 1Up/2Down, 1Up/3Down, 1Up/4Down,  
ratios             = [0.2845, 0.5488, 0.7393, 0.8415];
thresholds         = [77.85, 80.35, 83.15, 85.84];
stepStairDirection = minStep;

%% ESTIMATE STEP SIZE UP

ratio              = ratios(nStepSameDir);
T                  = thresholds(nStepSameDir);
ratioStep          = round(stepStairDirection/ratio);

end