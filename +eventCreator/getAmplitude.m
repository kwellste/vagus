function A = getAmplitude(expInfo,dataFile)

% -----------------------------------------------------------------------
% getAmplitude.m gets the amplitude, which will be used to stimulate in the 
%                'stim' task. This function helps ensure that the study is
%                double blind regarding stimulation protocol
%   
%   SYNTAX:       A = getAmplitude(expInfo,dataFile)
%
%   IN:           expInfo:  struct, contains visitType determined by
%                                   getVisitType.m
%                 dataFile: struct, data file initiated in initDataFile.m
%
%   OUT:          A:        integer, amplitude which will be used by the
%                                   'stim' task
%
%   AUTHOR:     Coded by: Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%

deltaBelow = (dataFile.stair.threshold - dataFile.stair.minValue)/5;
deltaAbove = (dataFile.stair.maxValue - dataFile.stair.threshold)/5;

%% indicate amplitude and stimulation mode for visitType

switch expInfo.visit.type
    case {'belowT'}
        dataFile.stim.amplitude = dataFile.stair.threshold - deltaBelow;
        A = dataFile.stim.amplitude;
    case {'atT'}
        dataFile.stim.amplitude = dataFile.stair.threshold;
        A = dataFile.stim.amplitude;
    case {'aboveT'}
        dataFile.stim.amplitude = dataFile.stair.threshold + deltaAbove;
        A = dataFile.stim.amplitude;
    case {'sham'}   
        dataFile.stim.amplitude = dataFile.stair.threshold;
        A = dataFile.stim.amplitude;
end


end