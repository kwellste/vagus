function expInfo = getVisitType(expInfo)

% -----------------------------------------------------------------------
% getVisitType.m gets the visit type of the current participant, which is
%                stored in an excel file. This function helps ensure that
%                the study is double blind regarding stimulation protocol
%   
%   SYNTAX:       expInfo = getVisitType(expInfo)
%
%   IN:           expInfo:  struct, contains key info on how the experiment 
%                                   is run,containing visit numer and PPID
%
%   OUT:          expInfo: struct, updated expInfo containing visitType
%
%   SUBFUNCTIONS: GetSecs.m; wait2.m adaptiveStimulation.m;
%                 simulateStimAmps.m; logEvent.m; logData.m;
%                 plotAmplitudes.m
%
%   AUTHOR:     Coded by: Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------

[~,~,raw]       = xlsread(fullfile(expInfo.randomisationPath, 'randomisation.xlsx'));
rowIdx          = find(strcmp(raw(:,1),expInfo.PPID));

rowIdx          = find(strcmp(raw(:,1),expInfo.PPID));
expInfo.VisitNo = ['visit' num2str(expInfo.visit.number)];
colIdx          = find(strcmp(raw(1,:),expInfo.visit.number));

expInfo.visit.type  = raw(rowIdx,colIdx);

if strcmp(expInfo.visit.type,'sham')
    expInfo.pStim.mode = 'sham';
else
    expInfo.pStim.mode = 'verum';
end


end