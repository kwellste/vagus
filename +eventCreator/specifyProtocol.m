function protocol = specifyProtocol

% -----------------------------------------------------------------------
% specifyProtocol.m creates the experiment protocol, i.e. the sequence of
%                   how the tasks will be run in this protocol.
%                   Adjust if you would like to amend the experimental protocol,
%                   e.g. task order, number of stages in experimet, etc.
%
%   SYNTAX:     protocol = specifyProtocol
%
%   OUT:        protocol: struct, contains task types and durations
%
%   AUTHOR:     Coded by: Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%

protocol.task(1).type       = 'rest';
protocol.task(1).maxDur     =  5000; %900000; 
protocol.task(2).type       = 'painDetect';
protocol.task(2).maxDur     =  60000; %900000;  
protocol.task(3).type       = 'stair';
protocol.task(3).maxDur     =  120000; %1200000; 
protocol.task(4).type       = 'calib';
protocol.task(4).maxDur     =  60000; %900000; 
protocol.task(5).type       = 'break';
protocol.task(5).maxDur     =  5000; %1800000; 
protocol.task(6).type       = 'stim';
protocol.task(6).maxDur     =  18000; %1200000; 
protocol.task(6).stopCrit   = 'time';

protocol.stage(1).Num       = 1;
protocol.stage(1).task      = 'rest';
protocol.stage(1).maxDur    = protocol.task(1).maxDur;
protocol.stage(1).stopCrit  = protocol.task(1).stopCrit;
protocol.stage(1).taskDone  = 0;
protocol.stage(2).Num       = 2;
protocol.stage(2).task      = 'painDetect';
protocol.stage(2).maxDur    = protocol.task(2).maxDur;
protocol.stage(2).stopCrit  = protocol.task(2).stopCrit;
protocol.stage(2).taskDone  = 0;
protocol.stage(3).Num       = 3;
protocol.stage(3).task      = 'stair';
protocol.stage(3).maxDur    = protocol.task(3).maxDur;
protocol.stage(3).stopCrit  = protocol.task(3).stopCrit;
protocol.stage(3).taskDone  = 0;
protocol.stage(4).Num       = 4;
protocol.stage(4).task      = 'calib';
protocol.stage(4).maxDur    = protocol.task(4).maxDur;
protocol.stage(4).stopCrit  = protocol.task(4).stopCrit;
protocol.stage(4).taskDone  = 0;
protocol.stage(5).Num       = 5;
protocol.stage(5).task      = 'break';
protocol.stage(5).maxDur    = protocol.task(5).maxDur;
protocol.stage(5).stopCrit  = protocol.task(5).stopCrit;
protocol.stage(5).taskDone  = 0;
protocol.stage(6).Num       = 6;
protocol.stage(6).task      = 'rest';
protocol.stage(6).maxDur    = protocol.task(1).maxDur;
protocol.stage(6).stopCrit  = protocol.task(1).stopCrit;
protocol.stage(6).taskDone  = 0;
protocol.stage(7).Num       = 7;
protocol.stage(7).task      = 'stim';
protocol.stage(7).maxDur    = protocol.task(6).maxDur;
protocol.stage(7).stopCrit  = protocol.task(6).stopCrit;
protocol.stage(7).taskDone  = 0;
protocol.stage(8).Num       = 8;
protocol.stage(8).task      = 'rest';
protocol.stage(8).maxDur    = protocol.task(1).maxDur;
protocol.stage(8).stopCrit  = protocol.task(1).stopCrit;
protocol.stage(8).taskDone  = 0;
protocol.stage(9).Num       = 9;
protocol.stage(9).task      = 'stim';
protocol.stage(9).maxDur    = protocol.task(6).maxDur;
protocol.stage(9).stopCrit  = protocol.task(6).stopCrit;
protocol.stage(9).taskDone  = 0;
protocol.stage(10).Num      = 10;
protocol.stage(10).task     = 'rest';
protocol.stage(10).maxDur    = protocol.task(1).maxDur;
protocol.stage(10).stopCrit  = protocol.task(1).stopCrit;
protocol.stage(10).taskDone = 0;

end