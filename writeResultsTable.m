function writeResultsTable(result, experimentInfo, relevantStages, foldername, fileending)

    % check inputs
    if nargin < 5
        foldername   = 'result';
    end

    if nargin < 6
        fileending = '.csv';
    end
    
    % transform relevantStages from row vector into column vector
    if size(relevantStages,2) > 1
        relevantStages = relevantStages.';
    end

    % validate input
    assert(isstruct(result), 'experiment:writeResultsTable:result', ...
        'Make sure that the result is of type struct.');
    
    % set filepath
    resultsfolder = sprintf('%s_%s', foldername, datestr(now, 'yymmdd_HHMM'));
    filepath = [pwd, filesep, resultsfolder, filesep];
    mkdir(filepath);

    % get result information
    resultTaskStages = result([result.trialNr] > 0);
    
    relStageDataTables = cell(numel(relevantStages), 1);

    for currentStageData = 1:numel(relevantStages)
        relStageDataTables{currentStageData} = ...
            struct2table([resultTaskStages(strcmp({resultTaskStages.type}, ...
                          relevantStages{currentStageData})).stageData]);
    end
    
    % adding total resultTable
    dataTables = [{struct2table(result)}; relStageDataTables];
    tableNames = [{'allStages'}; relevantStages];
    
    % restruct all stages
    resultStructures = cell(experimentInfo.nrRuns, experimentInfo.nrTasks);
    for currentRun = 1:size(resultStructures, 1)
        resultRunStages = resultTaskStages([resultTaskStages.runNr] == currentRun);
        for currentTask = 1:size(resultStructures, 2)
            resultRunTask = resultRunStages([resultRunStages.taskID] == currentTask).';
            
            resultStructures{currentRun, currentTask} = ...
                reshape(resultRunTask, experimentInfo.nrTrials, []);
        end
    end
    
    % write tables
    for currentTable = 1:numel(dataTables)
        filename = sprintf('%s', tableNames{currentTable});
        writetable(dataTables{currentTable}, sprintf('%s%s%s', filepath, filename, fileending));
    end
    
    % save matlab results file
    save([filepath, 'sourceResultData.mat'], ...
        'result', 'experimentInfo', 'resultStructures');
end
