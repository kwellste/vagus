function [options,abort] = checkEscape(options,expInfo,dataFile,trial)

% -----------------------------------------------------------------------
% checkEscape.m checks if the escape key was pressed and aborts the game in
%               that case
%
%   SYNTAX:       [dataFile, expInfo] = runTask(cues,options,expInfo,dataFile,...
%                                               task,maxDur)
%
%   IN:           options:  struct,  options the tasks will run with
%                 expInfo:  struct,  contains key info on how the experiment is 
%                                    run instance 
%                 dataFile: struct,  data file initiated in initDataFile.m
%                 trial:    integer, trial number
%
%   OUT:          options:  struct, updated options struct
%                 abort:    logical, returns = 1 if the task is aborted
%
%   AUTHOR:       Coded by: Frederike Petzschner, April 2017
%	              Amended:  Kathatina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%

keyCode = eventListener.commandLine.detectKey(expInfo.KBNumber, options.doKeyboard);

if isempty(trial)
    trial = 1;
end

if any(keyCode==options.keys.escape)
        sca
        tools.logEvent('exp','abort',dataFile,X,trial);
        tools.printAmp2txt(0, options.stair.stimType);
        eventListener.commandLine.wait2(3)
        abort = 1;
        stimulation.stopStim(sObj,device_paramsObj);
        disp('Experiment was aborted.')
        PsychPortAudio('DeleteBuffer');
        PsychPortAudio('Close');
        return;
else
    abort = 0;
end

end
