function keyCode = detectKey(KBNumber, doKeyboard)

% -----------------------------------------------------------------------
% detectKey.m checks if there is an input either from the keyboard or from the
%              response box 
%
%   SYNTAX:     keyCode = detectKey(expInfo, options)
%
%   IN:         KBNumber:   integer, number of the first found keyboard device
%               doKeyboard: logical, set to 1 if task is done on computer (as
%                                    opposed to response box)
%
%   OUT:        keyCode: vector of numbers corresponding to the pressed keys
%
%   AUTHOR:     Coded by:  Frederike Petzschner, April 2017
%               Amended:  Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
 
if doKeyboard == 0
    % EEG
    [~, keyCode, ~] = PsychRTBox('GetSecs', store.rtbox.rthandle);
    % also check keyboard in case of an escape
    [ ~, ~, keyCode2,  ~] = KbCheck(KBNumber);
    keyCode2 = find(keyCode2);
    keyCode = [keyCode, keyCode2];
    else
    %[ ~, ~, keyCode,  ~] = KbCheck(deviceNumber); % we commented this out
    % because this results in an error in Linux and MacOS Mojave
    [ ~, ~, keyCode,  ~] = KbCheck;
    keyCode = find(keyCode);
end

