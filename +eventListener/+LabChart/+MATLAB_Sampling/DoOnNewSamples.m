function DoOnNewSamples(varargin)
%Example event handler called when sampling, whenever new samples might be 
%available, typically 20 times a second.
%This example gets any new samples from channel gChan, appends them to
%the gChan1Data vector, then plots the latest 5000 samples.
global gLCDoc;
global gLatestBlock;
global gBlockSecsPerTick;
global gLatestTickInBlock;
global gChans;
global gChansData;
global gT;
%disp('OnNewSamples called')
% gLatestTickInBlock = gLatestTickInBlock+double(varargin{3});
% HRESULT GetChannelData([in]ChannelDataFlags flags, [in]long channelNumber, [in]long blockNumber, [in]long startSample, [in]long numSamples, [out,retval]VARIANT *data) const;
% For the sampling case we can pass -1 for the number of samples parameter,
% meaning return all available samples
newSamplesMax = 0; %max new samples added across channels
minChanLength = 1e30; %number of samples in the shortest channel
for ch = gChans
% HRESULT GetChannelData([in]ChannelDataFlags flags, [in]long channelNumber, [in]long blockNumber, [in]long startSample, [in]long numSamples, [out,retval]VARIANT *data) const;
% For the sampling case we can pass -1 for the number of samples parameter,
% meaning return all available samples
    chanSamples = gLCDoc.GetChannelData (1, ch, gLatestBlock+1, length(gChansData{ch})+1, -1);
    gChansData{ch} = [gChansData{ch}; chanSamples'];
    if minChanLength > length(gChansData{ch})
        minChanLength = length(gChansData{ch});
    end
    if newSamplesMax < length(chanSamples)
        newSamplesMax = length(chanSamples);
    end
end

if newSamplesMax > 0
    nSamples = length(gT);
    gT = [gT; [nSamples:nSamples+newSamplesMax-1]'*gBlockSecsPerTick];
end

%plot the latest 5000 samples
plotRange = max(1,minChanLength-5000):minChanLength;
for ch = gChans
    subplot(length(gChans),1,ch), plot(gT(plotRange),gChansData{ch}(plotRange));
end