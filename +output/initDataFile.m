function dataFile = initDataFile

% -----------------------------------------------------------------------
% initDataFile.m initializes the datafile for this experiment
%
%   SYNTAX:       dataFile = initDataFile
%
%   OUT:          dataFile: struct, contains all variables, for which data
%                                   will be saved
%
%   SUBFUNCTIONS: GetSecs.m
%
%   AUTHOR:     Based on: Frederike Petzschner & Sandra Iglesias, 2017
%               Amended:  Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%
%% EVENTS 
% Time stamps for on and offsets and special occurences (e.g. "abort event")

dataFile.events.exp_startTime           = GetSecs();
dataFile.events.exp_startProtocol       = [];
dataFile.events.intro_end               = [];
    
dataFile.events.rest_1_on               = [];
dataFile.events.rest_1_off              = [];

dataFile.events.painDetect_on           = [];
dataFile.events.painDetect_off          = [];
dataFile.events.painDetect_stimOn       = [];
dataFile.events.painDetect_stimOff      = [];
dataFile.events.painDetect_amplitudeOn  = [];
dataFile.events.painDetect_amplitudeOff = [];
dataFile.events.painDetect_responseOn   = [];
dataFile.events.painDetect_responseOff  = [];

dataFile.events.stair_on                = [];
dataFile.events.stair_off               = [];
dataFile.events.stair_stimOn            = [];
dataFile.events.stair_stimOff           = [];
dataFile.events.stair_amplitudeOn       = [];
dataFile.events.stair_amplitudeOff      = [];
dataFile.events.stair_responseOn        = [];
dataFile.events.stair_responseOff       = [];

dataFile.events.calib_on                = [];
dataFile.events.calib_off               = [];
dataFile.events.calib_stimOn            = [];
dataFile.events.calib_stimOff           = [];
dataFile.events.calib_amplitudeOn       = [];
dataFile.events.calib_amplitudeOff      = [];
dataFile.events.calib_responseOn        = [];
dataFile.events.calib_responseOff       = [];

dataFile.events.rest_2_on               = [];
dataFile.events.rest_2_off              = [];

dataFile.events.stim_1_on               = []; 
dataFile.events.stim_1_off              = [];

dataFile.events.rest_3_on               = [];
dataFile.events.rest_3_off              = [];

dataFile.events.stim_2_on               = []; 
dataFile.events.stim_2_off              = [];

dataFile.events.rest_4_on               = [];
dataFile.events.rest_4_off              = [];

dataFile.events.exp_abort               = [];
dataFile.events.exp_missedTrial         = []; logical(dataFile.events.exp_missedTrial);
dataFile.events.exp_stopCriterion       = []; logical(dataFile.events.exp_stopCriterion);
dataFile.events.exp_timeOut             = []; logical(dataFile.events.exp_timeOut);
dataFile.events.exp_amplitudeReset      = []; logical(dataFile.events.exp_amplitudeReset);
dataFile.events.exp_end                 = [];

%% TASK DATA
% data of the individual tasks, e.g. amplitudes, responses, etc.

%                               PAIN DETECTION

dataFile.painDetect.rt                  = [];
dataFile.painDetect.amplitude           = zeros(1000,2);
dataFile.painDetect.response            = zeros(1000,2);
dataFile.painDetect.detectThreshold     = [];
dataFile.painDetect.painThreshold       = [];

%                                 STAIRCASE

dataFile.stair.rt                       = [];
dataFile.stair.amplitude                = zeros(1000,2);
dataFile.stair.minAmplitude             = [];
dataFile.stair.maxAmplitude             = [];
dataFile.stair.targetedAmplitude        = zeros(1000,2);
dataFile.stair.reversals                = [];
dataFile.stair.response                 = zeros(1000,2);
dataFile.stair.threshold                = [];
dataFile.stair.stairType                = [];


%                               CALIBRATION

dataFile.calib.rt                       = [];
dataFile.calib.amplitude                = zeros(1000,2);
dataFile.calib.response                 = zeros(1000,2);
dataFile.calib.detectThreshold          = [];


%                               STIMULATION

dataFile.stim.amplitude                 = [];
dataFile.stim.response1stRun            = zeros(10,2);
dataFile.stim.response2ndRun            = zeros(10,2);

end