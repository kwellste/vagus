function dataFile = runCalibration(cues,options,expInfo,dataFile,task,maxDur)
%
% -----------------------------------------------------------------------
% runCalibration.m runs the pain detection calibration, where
%                  participants have to indicate when stimulation becomes 
%                  painful
%   
%   SYNTAX:       dataFile = tasks.runPainDetection(cues,options,expInfo,...
%                                                 dataFile,maxDur,stopCrit)
%
%   IN:           cues:  struct, containing general  options and task specific
%                 options:  struct, options the tasks will run with
%                 expInfo:  struct, contains key info on how the experiment is 
%                                   run instance, incl. keyboard number 
%                 dataFile: struct, data file initiated in initDataFile.m
%                 task:     string, task name {'calib','stair','painDetection'}
%                 maxDur:   double, maximum duration this task should run for,
%                                   specified in specifyProtocol
%
%   OUT:          dataFile: struct, updated data file
%
%   SUBFUNCTIONS: GetSecs.m; adaptiveStimulation.m; stimulation.simulateStimAmps.m
%                 logEvent.m; wait2.m
%
%   AUTHOR:       coded by:    Frederike Petzschner, April 2017
%                 last change: Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%

%% INITIALIZE calibration
stepping = 1;
nStep    = 0;
tic();

options.calib.amplitudeStart = min(dataFile.stair.amplitude);
options.calib.amplitudeStart = options.calib.amplitudeStart(1);
dataFile.events.calib_on     = GetSecs();

%% DO calibration
% start increasing
while stepping  
    nStep = nStep + 1; % next step
    
    if nStep == 1 %first step
        A = options.calib.amplitudeStart;
    end
    
    % stimulate
    if options.doInitStim
        [A,dataFile,resp] = stimulation.adaptiveStimulation(A,cues,ptions,...
                                           expInfo,dataFile,task,nStep);
        else
        [A,dataFile,resp] = stimulation.simulateStimAmps(A,cues,options,...
                                           expInfo,dataFile,task,nStep);
    end
    
    % find next amplitude
    response = resp;
    
    if response == 0  % stimulation not detected
        A = A + options.calib.stepSize;     % update amplitude
        
        if  A > options.calib.amplitudeMax  % check if amplitude is too high
            A = options.calib.amplitudeMax;
            amplitudeReset = 1;
            warning(['Amplitude too high, was reset to ', num2str(options.calib.amplitudeMax)])
            
           else
            amplitudeReset = 0;
        end
        
     elseif response == 1 % stimulation detected
         
            if nStep == 1
                A = A - options.calib.stepSize;
                stepping = 1;
            else
                            
                dataFile.calib.detectThreshold    = A;
                stepping = 0;
            end
    end
    
% check if timeout was reached    
    tocID = toc();
    
    if tocID > maxDur
        DrawFormattedText(options.screen.windowPtr, options.messages.timeOut, 'center', 'center', options.screen.black)
        Screen('Flip', options.screen.windowPtr)
        stepping = 0;
        dataFile.calib.detectThreshold = options.calib.amplitudeMax;
        warning(['Task time out, pain detection Threshold was set to ', num2str(options.calib.amplitudeMax)])
        timeOut = 1;
        stopCriterion = 0;
    else
        timeOut = 0;
        stopCriterion = 1;
    end
    
end % END calibration loop

%% END calibration

dataFile.events.calib_off = GetSecs();

% show end screen
Screen('DrawTexture', options.screen.windowPtr, cues.thankYou, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showOff,options,expInfo,dataFile,nStep);

Screen('DrawTexture', options.screen.windowPtr, cues.endCalib, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);

% save and clean data
dataFile = eventListener.logEvent('exp','_timeOut',dataFile,timeOut);
dataFile = eventListener.logEvent('exp','_stopCriterion',dataFile,stopCriterion);
dataFile = eventListener.logEvent('exp','_amplitudeReset',dataFile,amplitudeReset);
dataFile = tools.cleanDataFields(dataFile,task,nStep);

%% PLOT amplitudes

if options.doPlot
    output.plotAmplitudes('calib','detect',expInfo.expMode,dataFile);
end
end