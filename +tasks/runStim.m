function [dataFile, expInfo] = runStim(cues,options,expInfo,dataFile,task,maxDur)

% -----------------------------------------------------------------------
% runCalibration.m runs the pain detection calibration, where
%                  participants have to indicate when stimulation becomes 
%                  painful
%   
%   SYNTAX:       dataFile = tasks.runPainDetection(cues,options,expInfo,...
%                                                 dataFile,maxDur,stopCrit)
%
%   IN:           cues:  struct, containing general  options and task specific
%                 options:  struct, options the tasks will run with
%                 expInfo:  struct, contains key info on how the experiment is 
%                                   run instance, incl. keyboard number 
%                 dataFile: struct, data file initiated in initDataFile.m
%                 task:     string, task name {'calib','stair','painDetection'}
%                 maxDur:   double, maximum duration this task should run for,
%                                   specified in specifyProtocol
%
%   OUT:          dataFile: struct, updated data file
%                 expInfo:  struct, updated expInfo file
%
%   SUBFUNCTIONS: GetSecs.m; adaptiveStimulation.m; stimulation.simulateStimAmps.m
%                 logEvent.m; wait2.m
%
%   AUTHOR:       coded by:    Frederike Petzschner, April 2017
%                 last change: Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%
%% INITIATE stimulation
stimulating = 1;

% show intro screen
Screen('DrawTexture', options.screen.windowPtr, cues.stim1, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showIntroScreen,options,expInfo,1);

Screen('DrawTexture', options.screen.windowPtr, cues.stim2, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.waitfornextkey(options,expInfo,dataFile);

% choose amplitude
A     = eventCreator.getAmplitude(expInfo,dataFile);

% Warning Screen & Countdown
Screen('DrawTexture', options.screen.windowPtr, cues.stimulationStart, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showReadyScreen,options,expInfo,dataFile,trial);

if options.doCountdown
Screen('DrawTexture', options.screen.windowPtr, cues.countdown1, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.countdown,options,expInfo,dataFile,trial);

Screen('DrawTexture', options.screen.windowPtr, cues.countdown2, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.countdown,options,expInfo,dataFile,trial);

Screen('DrawTexture', options.screen.windowPtr, cues.countdown3, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
[~,~,dataFile]  = eventListener.commandLine.wait2(options.dur.countdown,options,expInfo,dataFile,trial);
end

ticID = tic();

%% START stimulation

while stimulating
    if options.doInitStim
        stimulation.startStim(expInfo.pStim.pStim_serial,options.pStim.pStim_params,A,expInfo.pStim.mode);
        % Wait till Amplitude is adjusted on device
        [~,~,dataFile] = tools.wait2(options.dur.deltawin2stim,options,expInfo,dataFile,trial);
    end

Screen('DrawTexture', options.screen.windowPtr, cues.stimulationOn ,[], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
[~,~,dataFile] = tools.wait2(maxDur, options,expInfo,dataFile,trial);

% check what stim phase this is and save corresponding data
if isempty(dataFile.events.stim_1_on)
          dataFile = eventListener.logEvent(task,'_1_on',dataFile,[],[]);
    elseif isempty(dataFile.events.stim_2_on)
          dataFile = eventListener.logEvent(task,'_2_on',dataFile,[],[]);
    else
        disp('Error in the struct ''protocol.stage''')
end   

% show stimulation screen
Screen('DrawTexture', options.screen.windowPtr, cues.stimulationOn , [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
tools.wait2(options.dur.stimBurst,options,expInfo,dataFile,trial);

%% QUESTION every few minutes
time = GetSecs() - dataFile.event.stim_On;
trial = 1;
if time == ismember(time,[(maxDur/qTimings),(maxDur/(qTimings/nQs-2)),maxDur/(qTimings/(nQs-1)),(maxDur/(qTimings/nQs))])
     [dataFile,~,resp] = tools.showResponseScreen(cues,options,expInfo,dataFile,task,trial);
     trial + 1;
     if isempty(dataFile.events.stim_1_on)
        [~,dataFile]      = eventListener.logData(resp,task,'response1stRun',dataFile,trial);
     else
        [~,dataFile]      = eventListener.logData(resp,task,'response2ndRun',dataFile,trial);
     end
end

tocID = toc();
    if tocID       == maxDur
        stimulating = 0;
        if options.doInitStim
           stimulation.stopStim(options.pStim.pStim_serial,options.pStim.pStim_params);
           tools.printAmp2txt(0, params.path.amplitude, params.stair.stimtype)     
        end

    end
end


%% END stimulation
% show end screen depending on stage
Screen('DrawTexture', options.screen.windowPtr, cues.thankYou, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showOff,options,expInfo,dataFile,nStep);
        
if isempty(dataFile.events.stim_1_off)
        Screen('DrawTexture', options.screen.windowPtr, cues.endStimPhase1_1,...
                [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        eventListener.commandLine.wait2(options.dur.showScreen,options,expInfo);
        dataFile = eventListener.logEvent(task,'_1_off',dataFile,[],[]);
        Screen('DrawTexture', options.screen.windowPtr, cues.endStimPhase1_1_2,...
                [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);
    elseif isempty(dataFile.events.stim_2_off)
        Screen('DrawTexture', options.screen.windowPtr, cues.endStimPhase2_1,...
                [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        eventListener.commandLine.wait2(options.dur.showScreen,options,expInfo);
        dataFile = eventListener.logEvent(task,'_2_off',dataFile,[],[]);
        Screen('DrawTexture', options.screen.windowPtr, cues.endStimPhase2_1_2,...
                [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);
    else
      disp('Error in the struct ''protocol.stage''')
      dataFile = tools.cleanDataFields(dataFile,task,trial);
end

end
