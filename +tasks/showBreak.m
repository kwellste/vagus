function [expInfo, dataFile] = showBreak(cues,options,expInfo,dataFile,maxDur)

% -----------------------------------------------------------------------
% showBreak.m shows introduction slides to this task
%
%   SYNTAX:     showIntro(options,cues)
%
%   IN:         cues:     struct, contains names of slides
%               options:  struct, options the tasks will run with
%               expInfo:  struct, contains key info on how the experiment is 
%                                run in this instance
%               dataFile: struct, data file initiated in initDataFile.m
%               maxDur:   double, maximum duration this task should run for, 
%                                specified in specifyProtocol
%
%   OUT:        expInfo:  struct, updated expInfo file
%               dataFile: struct, updated data file
%
%   SUBFUNCTIONS: GetSecs.m, Screen.m; wait2.m; waitfornextkey.m
%
%   AUTHOR:     Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%

%% INTRODUCE Break
breakTime          = 1;
expInfo.startBreak = GetSecs();
tic;

while breakTime
% Break Slide
Screen('DrawTexture', options.screen.windowPtr, cues.break, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);


% Alert Experimenter that break will be over soon
tocID = toc();
  if tocID == maxDur/(5/4)
        warning(['BREAK WILL END IN ' num2str(maxDur/(5/4)) ' MIN']);
        WarnWave = [sin(1:.6:400), sin(1:.7:400), sin(1:.4:400)];
        Audio = audioplayer(WarnWave, 22050);
        play(Audio); play(Audio); play(Audio);
    elseif tocID == maxDur/(10/9)
        warning(['BREAK WILL END IN ' num2str(maxDur/(5/4)) ' MIN']);
        WarnWave = [sin(1:.6:400), sin(1:.7:400), sin(1:.4:400)];
        Audio = audioplayer(WarnWave, 22050);
        play(Audio);play(Audio);play(Audio);
        
    elseif tocID > maxDur
      breakTime = 0;
  end
end

%% END Break
Screen('DrawTexture', options.screen.windowPtr, cues.endBreak, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);

expInfo.stopBreak = GetSecs();

end

