function dataFile = runStaircase(cues,options,expInfo,dataFile,task,maxDur)

% -----------------------------------------------------------------------
% runStaircase.m runs the staircase task, participants have to indicate 
%                whether they feel the stimulation or not at given
%                amplitudes
%   
%   SYNTAX:       dataFile = runStaircase(cues,options,expInfo,dataFile,maxDur)
%
%   IN:           cues:     struct, contains names of slides initiated in
%                                 initiate Visuals
%                 options:  struct, options the tasks will run with
%                 expInfo:  struct, contains key info on how the experiment is 
%                                 run instance, incl. keyboard number 
%                 dataFile: struct, data file initiated in initDataFile.m
%                 task:     string, task name, here 'stair'
%                 maxDur:   double, maximum duration this task should run for,
%                                 specified in specifyProtocol
%
%   OUT:          dataFile: struct, updated data file
%
%   SUBFUNCTIONS: GetSecs.m; wait2.m adaptiveStimulation.m;
%                 simulateStimAmps.m; logEvent.m; logData.m;
%                 plotAmplitudes.m
%
%   AUTHOR:     Coded by: Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%
%% SHOW intro staircase
dataFile = tools.showStairIntro(dataFile,options,cues,expInfo);

%% INITIALIZE staircase
stepping    = 1;
trial       = 0;
reversalVec = [];
stay00      = [0 0];
stay000     = [0 0 0];

% Set stairType-dependent options
switch expInfo.stairType
    case 'simpleUp'
            stairDirection = +1; % up
            otherDirection = -1; % down
            sameAmplitude  =  0;
            direction      = stairDirection;
    case 'simpleDown'
            stairDirection = -1; % up
            otherDirection = +1; % down
            sameAmplitude  =  0;
            direction      = stairDirection;

    case 'interleaved'
        dataFile = tasks.runInterleavedStaircase(cues,options,expInfo,dataFile,task,maxDur);
        return;
end

%% START staircase
% start increasing and decreasing
% show intro staircase

tic;
while stepping
      trial   = trial + 1;
      
    if trial == 1
        A     = options.stair.startValue;
        dataFile.stair.amplitude(trial)         = options.stair.startValue;
         if A > dataFile.painDetect.painThreshold
            A = dataFile.painDetect.painThreshold - options.painDetect.stepSize;
            dataFile.stair.targetedAmplitude(trial) = A;
        end
    else
        dataFile.stair.targetedAmplitude(trial) = A;
        if A > dataFile.painDetect.painThreshold
           A = dataFile.painDetect.painThreshold - options.stair.minStep;
           dataFile.stair.targetedAmplitude(trial) = A;
        end
    end
          
    % stimulate and collect response
    if options.doInitStim
        [stimAmp,dataFile,response] = stimulation.adaptiveStimulation(A,cues,...
                                      options,expInfo,dataFile,'stair',trial); 
        [~,dataFile] = eventListener.logData(stimAmp,'stair','amplitude',dataFile,trial);
        
        else
        [stimAmp,dataFile,response] = stimulation.simulateStimAmps(A,cues,...
                                      options,expInfo,dataFile,'stair',trial);
        [~,dataFile] = eventListener.logData(stimAmp,'stair','amplitude',dataFile,trial);
    end
    
    % print Amplitude to commandwindow
    switch expInfo.expMode
        case 'debug'
          disp(['trial: ',num2str(trial),' | stimulated with (mV): ',num2str(A),...
          ' | response: ',num2str(response),' | reversals so far: ',num2str(sum(reversalVec~=0))])
    end
    
%% INITIATE settings for staircase based on responses
 
       if trial == 1
           nLastSteps = 1;
       elseif trial == 2
           nLastSteps = 2;
       else
           nLastSteps = trial-(options.stair.nStepSameDir-1);
       end
    
    if strcmp(expInfo.stairType, 'simpleUp')
        if response == 1
            if trial == 1
                directionChanger   = 0;
                reversalVec(trial) = 0;
                direction          = stairDirection;
            else
                directionChanger   = 1;
                reversalVec(trial) = 1;
                direction          = otherDirection;
            end
        elseif response == 0 && nansum(reversalVec) == 0
            directionChanger   = 0;
            reversalVec(trial) = 0;
            direction          = stairDirection;
        elseif response == 0 && isequal(reversalVec(:,nLastSteps:end),stay00) 
            directionChanger   = 0;
            reversalVec(trial) = 1;
            direction          = stairDirection;
        elseif response == 0 && ~isequal(reversalVec(:,nLastSteps:end),stay00) 
            directionChanger   = 0;
            reversalVec(trial) = 0;
            direction          = sameAmplitude;
        end
        
elseif strcmp(expInfo.stairType, 'simpleDown')
       if response == 0
            if trial == 1
                directionChanger   = 0;
                reversalVec(trial) = 0;
                direction          = stairDirection;
            else
                directionChanger   = 1;
                reversalVec(trial) = 1;
                direction          = otherDirection;
            end
             if A > dataFile.painDetect.painThreshold
                A = dataFile.stair.targetedAmplitude(trial-1);
             end   
       elseif response == 1 && nansum(reversalVec) == 0
            directionChanger   = 0;
            reversalVec(trial) = 0;
            direction          = stairDirection;
        elseif response == 1 && isequal(reversalVec(:,nLastSteps:end),stay00) 
            directionChanger   = 0;
            reversalVec(trial) = 1;
            direction          = stairDirection;
        elseif response == 1 && ~isequal(reversalVec(:,nLastSteps:end),stay00)
            directionChanger   = 0;
            reversalVec(trial) = 0;
            direction          = sameAmplitude;
        end
    end
        
 %% STEP through Staircase 
% check the first reversal has occured yet, until the first reversal is
% indicated the staircase will only step in its ("simpleUp", -"Down")
% direction, after that it will change direction after nStepsSameDir is
% exceeded
    
    if nansum(reversalVec)    == 0  
       if direction == stairDirection  
            if stairDirection == -1   
                A = stimAmp - options.stair.stairDirStep;   
            else                     
                A = stimAmp + options.stair.stairDirStep;
                
            end 
       end 
       
    elseif nansum(reversalVec) > 0  
            if directionChanger      
                direction = otherDirection;
                
                if otherDirection == -1
                   A = stimAmp - options.stair.otherDirStep;
                else
                   A = stimAmp + options.stair.otherDirStep;  
                end
                
            else                    
                if direction == sameAmplitude 
                    if ~isequal(reversalVec(:,nLastSteps:end),stay000) % 5^Plateau: No plateau
                        A = stimAmp;
                    else
                       direction = stairDirection; % 0 reversals have occured 3 times in a row
                    end  % END 5^Plateau
                    
                 elseif direction == stairDirection 
                    if stairDirection == -1 
                      A = stimAmp - options.stair.stairDirStep; 
                    else
                      A = stimAmp + options.stair.stairDirStep;
                    end 
                    
                end 
            end
    end 
    
%% SAVE amplitudes suggested by staircase
    [~,dataFile] = eventListener.logData(stimAmp,'stair','amplitude',dataFile,trial);

%% CHECK stop and amplitude criteria
% criterionLoop      = check what criterion applies and if it was reached
% amplitudeCheckLoop = check of minimun or maximum amplitude was reached

    if strcmp(options.stair.stopCriterion,'reversals') && nansum(reversalVec~=0) == options.stair.stopRule % START criterionLoop
       reachedStop  = 1;
       stepping     = 0; % stop stepping through staircase
       
        elseif strcmp(options.stair.stopCriterion,'trials') && trial == options.stair.stopRule
        reachedStop = 1;
        stepping    = 0; % stop stepping through staircase
        else
        reachedStop  = 0;
        
    end % END criterionLoop
    
    if ~reachedStop  
        % check if pain Threshold reached and reset maxAmplitude one step below
        if A == dataFile.painDetect.painThreshold
           options.stair.maxValue = dataFile.painDetect.painThreshold - options.stair.stairDirStep;
           A = options.stair.maxValue;
           [~,dataFile] = eventListener.logData(A,'stair','targetedAmplitude',dataFile,trial+2);
           amplitudeReset = 1; 
            elseif A > options.stair.maxValue
                   A = options.stair.maxValue;
                   [~,dataFile] = eventListener.logData(A,'stair','targetedAmplitude',dataFile,trial+2);
                   amplitudeReset = 1;  
                   
            elseif A < options.stair.minValue
                   A = options.stair.minValue;
                   [~,dataFile] = eventListener.logData(A,'stair','targetedAmplitude',dataFile,trial+2);
                   amplitudeReset = 1;
        else
                   amplitudeReset = 0; 
        end % END amplitudeCheckLoop 
        
    end  % END criterion CheckLoop 
    
    % check if timeOut reached
    tocID = toc;
    if tocID > maxDur
        DrawFormattedText(options.screen.windowPtr, options.messages.timeOut, 'center', 'center', options.screen.black)
        Screen('Flip', options.screen.windowPtr)
        stepping                 = 0;
        dataFile.stair.threshold = stimAmp;
        warning(['Task time out, detection Threshold was set to ', num2str(stimAmp)])
        dataFile = eventListener.logEvent('exp','_timeOut',dataFile,[],trial);
    end

end

%% END staircase
% show end screen
Screen('DrawTexture', options.screen.windowPtr, cues.thankYou, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showOff,options,expInfo,dataFile,trial);

Screen('DrawTexture', options.screen.windowPtr, cues.endStair1 , [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showScreen,options,expInfo,dataFile,trial);

% save data
dataFile.stair.reversals = reversalVec;
dataFile.stair.threshold = nanmean(dataFile.stair.amplitude((trial-options.stair.stopRule):trial,1));
dataFile.stair.maxValue  = options.stair.maxValue;
dataFile.stair.minValue  = options.stair.minValue;
dataFile = eventListener.logEvent('exp','_amplitudeReset',dataFile,amplitudeReset,[]);
dataFile = tools.cleanDataFields(dataFile,task,trial);

%% PLOT staircase

if options.doPlot
    output.plotAmplitudes('stair',expInfo.stairType,expInfo.expMode,dataFile)
end

end

