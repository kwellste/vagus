function dataFile = runRest(cues,options,expInfo,dataFile,task,maxDur)

% -----------------------------------------------------------------------
% runRest.m runs 'Rest' Task
%
%   SYNTAX:       dataFile = runRest(cues,options,expInfo,dataFile,task,maxDur)
%
%   IN:           cues:     struct, contains names of slides initiated in
%                                   initiate Visuals
%                 options:  struct, options the tasks will run with
%                 expInfo:  struct, contains key info on how the experiment is 
%                                   run instance, incl. keyboard number
%                 dataFile: struct, data file initiated in initDataFile.m
%                 task:     string, task name = 'rest'
%                 maxDur:   integer, maximum duration this task should run for, 
%                                   specified in specifyProtocol
%
%   OUT:          dataFile: struct, updated data file
%
%   SUBFUNCTIONS: Screen.m; wait2.m; waitfornextkey.m;logEvent.m
%
%   AUTHOR:       Based on: Frederike Petzscher, April 2017
%                 Amended:  Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------

%% INTRODUCE Rest Phase
Screen('DrawTexture', options.screen.windowPtr, cues.rest1, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
[~,~,dataFile] = eventListener.commandLine.wait2(options.dur.showIntroScreen,options,expInfo,dataFile,[]);

Screen('DrawTexture', options.screen.windowPtr, cues.rest2, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);

%% SHOW Rest Phase
Screen('DrawTexture', options.screen.windowPtr, cues.fixation, [], options.screen.rect, 0);

% check what rest phase this is and save corresponding data
if isempty(dataFile.events.rest_1_on)
          dataFile  = eventListener.logEvent(task,'_1_on',dataFile,[],[]);
    elseif isempty(dataFile.events.rest_2_on)
          dataFile  = eventListener.logEvent(task,'_2_on',dataFile,[],[]);
    elseif isempty(dataFile.events.rest_3_on)
          dataFile  = eventListener.logEvent(task,'_3_on',dataFile,[],[]);
    elseif isempty(dataFile.events.rest_4_on)
           dataFile = eventListener.logEvent(task,'_4_on',dataFile,[],[]);
    else
        disp('Error in the struct ''protocol.stage''')
end

% show screen for correct duration
Screen('Flip', options.screen.windowPtr);
[~,~,dataFile] = eventListener.commandLine.wait2(maxDur,options,expInfo,dataFile,[]);

% check what rest phase this is and show corresponding slide plus save
% corresponding data
Screen('DrawTexture', options.screen.windowPtr, cues.thankYou, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showOff,options,expInfo,dataFile,[]);

if isempty(dataFile.events.rest_1_off)
        Screen('DrawTexture', options.screen.windowPtr, cues.endRestPhase1_1,...
                [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        [~,~,dataFile] = eventListener.commandLine.wait2(options.dur.showOff,...
                                                 options,expInfo,dataFile,[]);
        dataFile       = eventListener.logEvent(task,'_1_off',dataFile,[],[]);
        Screen('DrawTexture', options.screen.windowPtr, cues.endRestPhase1_1_2,...
                [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        dataFile      = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);
        
    elseif isempty(dataFile.events.rest_2_off)
        Screen('DrawTexture', options.screen.windowPtr, cues.endRestPhase2_1,...
                [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        [~,~,dataFile] = eventListener.commandLine.wait2(options.dur.showOff,...
                                                 options,expInfo,dataFile,[]);
        dataFile       = eventListener.logEvent(task,'_2_off',dataFile,[],[]);
        Screen('DrawTexture', options.screen.windowPtr, cues.endRestPhase2_1_2,...
                [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        dataFile       = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);
        
    elseif isempty(dataFile.events.rest_3_off)
        Screen('DrawTexture', options.screen.windowPtr, cues.endRestPhase3_1,...
                [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        [~,~,dataFile] = eventListener.commandLine.wait2(options.dur.showOff,...
                                                 options,expInfo,dataFile,[]);
        dataFile       = eventListener.logEvent(task,'_3_off',dataFile,[],[]);
        Screen('DrawTexture', options.screen.windowPtr, cues.endRestPhase3_1_2,...
                [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        dataFile       = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);
        
    elseif isempty(dataFile.events.rest_4_off)
        Screen('DrawTexture', options.screen.windowPtr, cues.endExp,...
                [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        [~,~,dataFile] = eventListener.commandLine.wait2(options.dur.showScreen,...
                                                 options,expInfo,dataFile,[]);
        dataFile       = eventListener.logEvent(task,'_4_off',dataFile,[],[]);

    else
        disp('Error in the struct ''protocol.stage''')
end

end

