function dataFile = runInterleavedStaircase(cues,options,expInfo,dataFile,task,maxDur)

% -----------------------------------------------------------------------
% runInterleavedStaircase.m runs the staircase in interleaved fashion, 
%                            changing from a staircase going upward ('simpleUp')
%                            to a staircase going down ('simpleDown'),
%                            participants have to indicate whether they
%                            feel the stimulation
%   
%   SYNTAX:       dataFile = runInterleavedStaircase(cues,options,expInfo,...
%                                                    dataFile,maxDur)
%
%   IN:           cues:     struct, contains names of slides initiated in
%                                    initiate Visuals
%                 options:  struct, options the tasks will run with
%                 expInfo:  struct, contains key info on how the experiment is 
%                                   run instance, incl. keyboard number 
%                 dataFile: struct, data file initiated in initDataFile.m
%                 task:     string, task name, here 'stair'
%                 maxDur:   double, maximum duration this task should run for,
%                                 specified in specifyProtocol
%
%   OUT:          dataFile: struct, updated data file
%
%   SUBFUNCTIONS: GetSecs.m; wait2.m adaptiveStimulation.m;
%                 simulateStimAmps.m; logEvent.m; logData.m;
%                 plotAmplitudes.m
%
%   AUTHOR:     Coded by: Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%

%% INITIALIZE staircase
stepping        = 1;
trial           = 0;
reversalVec     = []; % vector of all reversals
reversalVecUp   = []; % vector of reversals in up staircase
reversalVecDown = []; % vector of reversals in down staircase
stairType       = []; 
stay00          = [0 0];   % vectors for testing if a stair plateau is indicated
stay000         = [0 0 0]; % i.e. if a vector of consecutive 0s is true for the reversalVecs

%% START staircase
% start increasing and decreasing
tic;
while stepping
      trial  = trial + 1;
      
    if trial == 1
        A = options.stair.startValueUp;  % Up-Staircase
        dataFile.stair.targetedAmplitude(trial)   = options.stair.startValueUp;
        dataFile.stair.amplitude(trial)           = options.stair.startValueUp;
        stairDirection   = +1;
        otherDirection   = -1; 
        sameAmplitude    =  0;
        direction        = stairDirection;
        stairType(trial) = +1;
        trialUp          = 1;
        
    elseif trial == 2   
        A = options.stair.startValueDown;  % Down-Staircase
        dataFile.stair.targetedAmplitude(trial)    = options.stair.startValueDown;
        dataFile.stair.amplitude(trial)            = options.stair.startValueDown;
        if A > dataFile.painDetect.painThreshold
            A = dataFile.painDetect.painThreshold - options.painDetect.stepSize;
            dataFile.stair.targetedAmplitude(trial) = A;
        end
        stairDirection   = -1; % up
        otherDirection   = +1; % down
        sameAmplitude    =  0;
        direction        = stairDirection;
        stairType(trial) = -1;
        trialDown        = 1;
        
    elseif stairType(trial-2) == +1 % uneven trials: Up-Staircase
        A = dataFile.stair.targetedAmplitude(trial);
        stairDirection   = +1; 
        otherDirection   = -1; 
        sameAmplitude    =  0;
        stairType(trial) = +1;
        trialUp          = (trial+1)/2;
        
    elseif stairType(trial-2) == -1  % even trials: Down-Staircase
        A = dataFile.stair.targetedAmplitude(trial);
        stairDirection   = -1; % up
        otherDirection   = +1; % down
        sameAmplitude    =  0;
        stairType(trial) = -1;
        trialDown        = trial/2;
    end

    
    % stimulate and collect response
    if options.doInitStim
        [stimAmp,dataFile,response] = stimulation.adaptiveStimulation(A,cues,...
                                      options,expInfo,dataFile,'stair',trial); 
        [~,dataFile] = eventListener.logData(stimAmp,'stair','amplitude',dataFile,trial);
        else
        [stimAmp,dataFile,response] = stimulation.simulateStimAmps(A,cues,...
                                      options,expInfo,dataFile,'stair',trial);
        [~,dataFile] = eventListener.logData(stimAmp,'stair','amplitude',dataFile,trial);
    end
    
    % print Amplitude to commandwindow
    switch expInfo.expMode
        case 'debug'
          disp(['trial: ',num2str(trial),' | current staircase: ',num2str(stairType(trial)),...
              ' | stimulated with (mV): ',num2str(stimAmp),' | response: ',num2str(response),...
              ' | reversals so far: ',num2str(sum(reversalVec~=0))])
    end    
%% INITIATE settings for staircase based on responses
        
    if stairType(trial) == +1 % Up-Staircase
        if trial == 1
           nLastSteps = 1;
        else
           nLastSteps = trialUp-(options.stair.nStepSameDir-1);
        end
        if response == 1
            if trial == 1
                directionChanger       = 0;
                reversalVecUp(trialUp) = 0;
                reversalVec(trial)     = 0;
                direction              = stairDirection;
            else
                directionChanger       = 1;
                reversalVecUp(trialUp) = 1;
                reversalVec(trial)     = 1;
                direction              = otherDirection;
            end
        elseif response == 0 && nansum(reversalVecUp) == 0
            directionChanger       = 0;
            reversalVecUp(trialUp) = 0;
            reversalVec(trial)     = 0;
            direction              = stairDirection;
        elseif response == 0 && isequal(reversalVecUp(:,nLastSteps:end),stay00) 
            directionChanger       = 0;
            reversalVecUp(trialUp) = 1;
            reversalVec(trial)     = 1;
            direction              = stairDirection;
        elseif response == 0 && ~isequal(reversalVecUp(:,nLastSteps:end),stay00) 
            directionChanger       = 0;
            reversalVecUp(trialUp) = 0;
            reversalVec(trial)     = 0;
            direction              = sameAmplitude;
        end
        
elseif stairType(trial) == -1 % Down-Staircase
        if trial   == 2
           nLastSteps = 1;
        else
           nLastSteps = trialDown-(options.stair.nStepSameDir-1);
        end

       if response == 0
           if trial == 2
                directionChanger           = 0;
                reversalVecDown(trialDown) = 0;
                reversalVec(trial)         = 0;
                direction                  = stairDirection;
           else
                directionChanger           = 1;
                reversalVecDown(trialDown) = 1;
                reversalVec(trial)         = 1;
                direction                  = otherDirection;
           end
       elseif response == 1 && nansum(reversalVecDown) == 0
            directionChanger           = 0;
            reversalVecDown(trialDown) = 0;
            reversalVec(trial)         = 0;
            direction                  = stairDirection;
        elseif response == 1 && isequal(reversalVecDown(:,nLastSteps:end),stay00) 
            directionChanger           = 0;
            reversalVecDown(trialDown) = 1;
            reversalVec(trial)         = 1;
            direction                  = stairDirection;
        elseif response == 1 && ~isequal(reversalVecDown(:,nLastSteps:end),stay00)
            directionChanger           = 0;
            reversalVecDown(trialDown) = 0;
            reversalVec(trial)         = 0;
            direction                  = sameAmplitude;
        end
    end

%% STEP through Staircase
% check the first reversal has occured yet, until the first reversal is
% indicated the staircase will only step in its ("simpleUp", -"Down")
% direction, after that it will change direction after nStepsSameDir is
% exceeded

  
    if nansum(reversalVecDown)   == 0 && stairDirection == -1
            A = stimAmp - options.stair.stairDirStep;
            
    elseif nansum(reversalVecUp) == 0 && stairDirection == +1 
           A = stimAmp + options.stair.stairDirStep;

    elseif nansum(reversalVecUp) > 0 && stairDirection == +1 
        if directionChanger               
            direction = otherDirection;   
            A = stimAmp - options.stair.otherDirStep;
        else                            
            if direction == sameAmplitude  
                if ~isequal(reversalVecUp(:,nLastSteps:end),stay000) % 5^Plateau: No plateau
                     A = stimAmp;
                else
                    direction = stairDirection; 
                     A = stimAmp + options.stair.stairDirStep;
                end  % END 5^Plateau

            elseif direction == stairDirection 
                 A = stimAmp + options.stair.stairDirStep;
            end
        end

    elseif nansum(reversalVecDown) > 0 && stairDirection == -1
        if directionChanger               
            direction = otherDirection;   
             A = stimAmp + options.stair.otherDirStep;
        else                             
            if direction == sameAmplitude 
                if ~isequal(reversalVecDown(:,nLastSteps:end),stay000) % 5.2^Plateau: No plateau
                     A = stimAmp;
                else
                    direction = stairDirection;
                     A = stimAmp - options.stair.stairDirStep;
                end  % END 5.2^Plateau

            elseif direction == stairDirection
                 A = stimAmp - options.stair.stairDirStep;
            end
        end
    end 
%% save updated amplitude for next trial
   [~,dataFile] = eventListener.logData(A,'stair','targetedAmplitude',dataFile,trial+2);
   
%% CHECK stop and amplitude criteria
% criterionLoop      = check what criterion applies and if it was reached
% amplitudeCheckLoop = check of minimun or maximum amplitude was reached

    if strcmp(options.stair.stopCriterion,'reversals') && nansum(reversalVec~=0) == options.stair.stopRule % START criterionLoop
       reachedStop  = 1;
       stepping     = 0; % stop stepping through staircase
       
        elseif strcmp(options.stair.stopCriterion,'trials') && trial == options.stair.stopRule
        reachedStop = 1;
        stepping    = 0; % stop stepping through staircase
        else
        reachedStop = 0;
        
    end % END criterionLoop
    
    if ~reachedStop  
        % check if pain Threshold reached and reset maxAmplitude one step below
        if A > dataFile.painDetect.painThreshold
           options.stair.maxValue = dataFile.painDetect.painThreshold - options.stair.stairDirStep;
           A = dataFile.painDetect.painThreshold - options.stair.minStep;
           [~,dataFile] = eventListener.logData(A,'stair','targetedAmplitude',dataFile,trial+2);
           amplitudeReset      = 1;
            elseif A > options.stair.maxValue
                   A = options.stair.maxValue;
                   [~,dataFile] = eventListener.logData(A,'stair','targetedAmplitude',dataFile,trial+2);
                   amplitudeReset      = 1;
                   
            elseif A < options.stair.minValue
                   A = options.stair.minValue;
                   [~,dataFile] = eventListener.logData(A,'stair','targetedAmplitude',dataFile,trial+2);
                   amplitudeReset      = 1;
        else
                   amplitudeReset      = 0; 
        end % END amplitudeCheckLoop 
        
    end  % END criterion CheckLoop 
    
    % check if timeOut reached
    tocID = toc;
    if tocID > maxDur
        DrawFormattedText(options.screen.windowPtr, options.messages.timeOut,...
                          'center', 'center', options.screen.black)
        Screen('Flip', options.screen.windowPtr)
        stepping                 = 0;
        dataFile.stair.threshold = stimAmp;
        warning(['Task time out, detection Threshold was set to ', num2str(stimAmp)])
        dataFile = eventListener.logEvent('exp','_timeOut',dataFile,[],trial);
    end

end

%% END stepping through staircase
% Show end screen
Screen('DrawTexture', options.screen.windowPtr, cues.thankYou, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showOff,options,expInfo,dataFile,trial);

Screen('DrawTexture', options.screen.windowPtr, cues.endStair1 , [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showOff,options,expInfo,dataFile,trial);

%% save data
dataFile.stair.reversals      = reversalVec;
dataFile.stair.stairType      = stairType;
thresholdTrials               = dataFile.stair.amplitude((trial-(2*options.stair.stopRule)):trial,1);
dataFile.stair.threshold(1)   = nanmean(thresholdTrials);
dataFile.stair.threshold(2)   = nanmean(thresholdTrials((1:2:end)));
dataFile.stair.threshold(3)   = nanmean(thresholdTrials((2:2:end)));
dataFile.stair.maxValue       = options.stair.maxValue;
dataFile.stair.minValue       = options.stair.minValue;
dataFile = eventListener.logEvent('exp','_amplitudeReset',dataFile,amplitudeReset,[]);
dataFile = tools.cleanDataFields(dataFile,task,trial);

%% plot staircase
if options.doPlot
    output.plotAmplitudes('stair',expInfo.stairType,expInfo.expMode,dataFile);
end

end