function [dataFile, expInfo] = runTasks(cues,options,expInfo,dataFile,task,maxDur)

% -----------------------------------------------------------------------
% runTasks.m selects the correct function for the corresponding type of task
%            which will be run, based on the task string received from
%            runExperiment.m
%
%   SYNTAX:       [dataFile, expInfo] = runTask(cues,options,expInfo,dataFile,...
%                                               task,maxDur)
%
%   IN:           cues:     struct, contains names of slides initiated in
%                                   initiate Visuals
%                 expInfo:  struct, contains key info on how the experiment is 
%                                   run in this instance 
%                 dataFile: struct, data file initiated in initDataFile.m
%                 task:     string, task name received from runExperiment
%                 maxDur:   double, maximum duration this task should run for, 
%                                   specified in specifyProtocol
%
%   OUT:          dataFile: struct, updated data file 
%                 expInfo:  struct, updated expInfo file
%
%   AUTHOR:     Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%

switch task
    case 'rest'
        if options.doRest
        dataFile            = tasks.runRest(cues,options,expInfo,dataFile,task,maxDur);
        end
    case 'painDetect'
        if options.doPainDetection
        dataFile = tasks.runPainDetection(cues,options,expInfo,dataFile,task,maxDur);
        end
    case 'stair'
        if options.doStaircase
        dataFile            = tasks.runStaircase(cues,options,expInfo,dataFile,task,maxDur);
        Screen('DrawTexture', options.screen.windowPtr, cues.endStair1_2, [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);
        end
    case 'calib'
        if options.doCalibration
        dataFile            = tasks.runCalibration(cues,options,expInfo,dataFile,task,maxDur);
        end
    case 'break'
        if options.doBreak
        [expInfo, dataFile] = tasks.showBreak(cues,options,expInfo,dataFile,maxDur);
        Screen('DrawTexture', options.screen.windowPtr, cues.endPainDetect1_2, [], options.screen.rect, 0);
        Screen('Flip', options.screen.windowPtr);
        dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);
        end
    case 'stim'
        if options.doStimulation
        [dataFile,expInfo] = tasks.runStim(cues,options,expInfo,dataFile,task,maxDur);
        end
end

end