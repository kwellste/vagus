function [dataFile,expInfo,protocol] = runExperiment(cues,options,protocol,expInfo,dataFile)

% -----------------------------------------------------------------------
% runExperiment.m creates a struct with all the key data and info on the
%              current run of the experiment, i.e. the
%              computer it was run on, the date, the time it took, the errors 
%              detected,
%              etc.
%
%   SYNTAX:       [dataFile, expInfo,protocol] = runExperiment(cues,options, protocol,
%                                                              expInfo, dataFile)
%
%   IN:           options:  struct, options the tasks will run with
%                 protocol: struct, information on this experimental protocol, 
%                                   such as task sequence
%                 expInfo:  struct, contains key info on how the experiment is 
%                                   run instance
%                 dataFile: struct, data file initiated in initDataFile.m
%
%   OUT:          dataFile: struct, updated data file
%                 expInfo:  struct, updated expInfo file
%                 protocol: struct, updated information on this experimental 
%                                   protocol, such as what tasks were run
%
%   SUBFUNCTIONS: runTask.m
%
%   AUTHOR:     Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------

%% RUN through experiment stages
%initialize experiment stage

expStage = 1;
while protocol.stage(expStage).taskDone == 0
    task      = protocol.stage(expStage).task;
    maxDur    = protocol.stage(expStage).maxDur;
    dataFile  = tasks.runTasks(cues,options,expInfo,dataFile,task,maxDur);
    options.stage(expStage).taskDone     = 1;
    expStage  = expStage + 1;
end

end
