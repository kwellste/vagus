function dataFile = runPainDetection(cues,options,expInfo,dataFile,task,maxDur)

% -----------------------------------------------------------------------
% runPainDetection.m runs the pain detection calibration, where
%                    participants have to indicate when stimulation becomes 
%                    painful
%   
%   SYNTAX:       dataFile = runPainDetection(cues,options,expInfo,dataFile,...
%                                             task,maxDur)
%
%   IN:           cues:     struct, contains names of slides initiated in
%                                 initiate Visuals
%                 protocol: struct, information on this experimental protocol, 
%                                   such as task sequence
%                 options:  struct, options the tasks will run with
%                 expInfo:  struct, contains key info on how the experiment is 
%                                 run instance, incl. keyboard number 
%                 dataFile: struct, data file initiated in initDataFile.m
%                 task:     string, task name = 'painDetection'
%                 maxDur:   double, maximum duration this task should run for,
%                                 specified in specifyProtocol
%
%   OUT:          dataFile: struct, updated data file
%
%   SUBFUNCTIONS: GetSecs.m; runCalibration; wait2.m adaptiveStimulation.m;
%                 simulateStimAmps.m; logEvent.m; plotAmplitudes.m
%
%   AUTHOR:     Coded by: Frederike Petzschner & Sandra Iglesias, 2017-2018
%               Amended:  Katharina V. Wellstein, December 2019
% -------------------------------------------------------------------------
%

%% SHOW intro
wnnjScreen('DrawTexture', options.screen.windowPtr, cues.painDetect1, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showOff,options,expInfo,dataFile,1);

Screen('DrawTexture', options.screen.windowPtr, cues.painDetect2, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
dataFile = eventListener.commandLine.waitForNextKey(options,expInfo,dataFile);

%% INITIALIZE pain detection
stepping     = 1;
nStep        = 0;
go2nextCalib = 0;
tic();
dataFile.events.painDetect_on = GetSecs();

%% START calibrating for pain threshold
% start increasing

while stepping  
      nStep  = nStep + 1; % next step
    
    if nStep == 1         % first step
        A    = options.painDetect.amplitudeStart;
        if A < 0
            A = 0;
        end
    end
    
    % stimulate
    if isempty(dataFile.painDetect.detectThreshold)
        task = 'calib'; 
        if options.doInitStim
           [A,dataFile,resp] = stimulation.adaptiveStimulation(A,cues,options,expInfo,dataFile,task,nStep);
        else
           [A,dataFile,resp] = stimulation.simulateStimAmps(A,cues,options,expInfo,dataFile,task,nStep);
        end
    else
        task = 'painDetect';
        if options.doInitStim
           [A,dataFile,resp] = stimulation.adaptiveStimulation(A,cues,options,expInfo,dataFile,task,nStep);
        else
           [A,dataFile,resp] = stimulation.simulateStimAmps(A,cues,options,expInfo,dataFile,task,nStep);
        end
    end
    
    
    % find next amplitude
    response = resp;
    
    if response == 0  % stimulation not painful
        A = A + options.painDetect.stepSize;    % update amplitude
        
        if  A > options.painDetect.amplitudeMax % check if amplitude is too high
            A = options.painDetect.amplitudeMax;
            amplitudeReset = 1;
            warning(['Amplitude too high, was reset to ', num2str(options.painDetect.amplitudeMax)])
          else
            amplitudeReset = 0;
        end
        elseif response == 1 && go2nextCalib == 0
               dataFile.painDetect.detectThreshold = A;
               A = A + options.painDetect.stepSize;
               amplitudeReset = 0;
               stepping     = 1;
               go2nextCalib = 1;
               
        elseif response == 1 && go2nextCalib == 1
               dataFile.painDetect.painThreshold    = A;
               amplitudeReset = 0;
               stepping = 0;
    end
    
    tocID = toc();
    
    if tocID > maxDur
        DrawFormattedText(options.screen.windowPtr, options.messages.timeOut, 'center', 'center', options.screen.black)
        Screen('Flip', options.screen.windowPtr)
        stepping = 0;
        dataFile.painDetect.painThreshold = options.painDetect.amplitudeMax;
        warning(['Task time out, pain detection Threshold was set to ', num2str(options.painDetect.amplitudeMax)])
        timeOut = 1;
        stopCriterion = 0;
    else
        timeOut = 0;
        stopCriterion = 1;
    end
    
end

%% END pain detection calibration
% show end screen
dataFile.events.painDetection_off = GetSecs();

Screen('DrawTexture', options.screen.windowPtr, cues.thankYou, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showOff,options,expInfo,dataFile,nStep);

Screen('DrawTexture', options.screen.windowPtr, cues.endPainDetect1, [], options.screen.rect, 0);
Screen('Flip', options.screen.windowPtr);
eventListener.commandLine.wait2(options.dur.showOff,options,expInfo,dataFile,nStep);

% save data
dataFile.events.painDetect_off = GetSecs();
dataFile = eventListener.logEvent('exp','_timeOut',dataFile,timeOut,nStep);
dataFile = eventListener.logEvent('exp','_stopCriterion',dataFile,stopCriterion,nStep);
dataFile = eventListener.logEvent('exp','_amplitudeReset',dataFile,amplitudeReset,nStep);
options.calib.amplitudeMax = dataFile.painDetect.painThreshold;
dataFile = tools.cleanDataFields(dataFile,task,nStep);
dataFile = tools.calib2painDetect(dataFile);

protocol.stage(2).taskDone = 1;

%% PLOT data

if options.doPlot
    output.plotAmplitudes('painDetect','pain',expInfo.expMode,dataFile);
end

end