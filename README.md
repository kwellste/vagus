VAGUS staircase task

ABOUT THIS TASK CODE
The VAGUS study’s purpose is to determine individual stimulation detection thresholds auricular using vagus nerve stimulation. The entire study protocol consists or several subtasks, incl. a pain detection threshold task, a staircase task with different types of staircases, another calibration task, resting periods, and a constant stimulation task.

This code consists of a full study protocol. For the purposes of the “Scientific programming for neuroeconomic experiments” evaluation, the pain detection and the staircase task consecutively, as the first determines the maximum amplitude value of the latter.

HOW TO USE THIS CODE

1. In tools.prepEnvironment, add yourself as a user and indicate your OS. 

2. There are two ways to run this code.
	1)	run ‘vagus_study’ and manually enter the inputs, false inputs will be flagged and can be changed while this function is running. ‘The vagus_study’ will feed the essential information to ‘vagus_experiment’. You may try running different types of staircases, ‘simpleUp’, ’simpleDown’, or ‘interleaved’, the latter being the most complicated but most accurate.
		NOTE: Running this study in ‘debug’ mode will allow you to see how you are stepping through the staircase 			online, this is recommended.

	1)	run directly: vagus_experiment(‘debug’,’test’,1,’interleaved’) OR  vagus_experiment(‘debug’,’test’,1,’simpleUp’) OR vagus_experiment(‘debug’,’test’,1,’simpleDown’)

REFERENCES
Parts of this code have been set up by Frederike Petzschner and Sandra Iglesias (both TNU, ETHZ & UZH) in 2017-2018. Katharina Wellstein added and amended functions. The authors of each function are referenced in the function description.

OUTPUT
The staircase and pain detection functions are plotted and show the detection threshold, which was determined using the particular answers given during the staircase. Since the stimulation device is not available, this is merely an imaginary exercise.

TECHNICAL NOTES:
This code was tested with MATLAB2018b, MATLAB2018a, and MATLAB2016b. MATLAB2019b issues a MEX file error, which I was not able to fix stably.



# vagus

